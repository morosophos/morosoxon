# morosoxon

![build status](https://gitlab.com/morosophos/morosoxon/badges/master/pipeline.svg)

## Description
Collection of services, libraries and utilities for managing xonotic servers

### dpcolors
Library to work with strings which have darkplaces color codes in them.

### xinfra
Utility to manage deployments of Xonotic servers

### pk3rs
Library to extract data from .pk3, .bsp and .mapinfo files, and to generate .mapinfo files with settings specified in an overrides file.

### xonotic_db
Library to parse and dump Xonotic db files

### xagy
Rcon client. **DEPRECATED**

## License
All source code in this repo is licensed under GNU GPL v3 license (see LICENSE file for details).

