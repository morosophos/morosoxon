extern crate dpcolors;

use dpcolors::*;
// use std::env::args;
// use std::fs::read;
use std::io::Error;

fn main() -> Result<(), Error> {
    let dp_string = DPString::parse("^1this text should be red");
    println!("Stripped colors: {}", dp_string.to_none());
    println!("ANSI colors: {}", dp_string.to_ansi_24bit());
    println!("HTML full: {}", dp_string.to_html());
    println!("HTML truncated: {}", dp_string.to_html_truncated(25));

    // let fname = args().nth(1).expect("Please a file name as an argument");
    // let data = read(&fname)?;
    // let dps = DPString::parse(&String::from_utf8_lossy(&data)).unwrap();
    // print!("{}", dps.to_ansi_24bit());
    Ok(())
}
