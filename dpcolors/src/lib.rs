mod dp_string;
use serde::{Deserialize, Deserializer};

pub use dp_string::DPString;

impl<'de> Deserialize<'de> for DPString {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: Deserializer<'de>
    {
        let s = String::deserialize(deserializer)?;
        Ok(DPString::parse(s.as_str()))
    }
}
