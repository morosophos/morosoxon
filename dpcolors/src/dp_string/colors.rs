use std::{
    convert::TryFrom,
    hash::{Hash, Hasher},
};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum ColorError {
    #[error("Invalid dec color: {0}")]
    InvalidDecColor(u8),
    #[error("Invalid hex color: {0}")]
    InvalidHexColor(u8),
    #[error("Parse error: {0}")]
    ParseError(#[from] std::num::ParseIntError),
}

const DEC_COLORS: [(u8, u8, u8); 10] = [
    (51, 51, 51),
    (255, 0, 0),
    (51, 255, 0),
    (255, 255, 0),
    (51, 102, 255),
    (51, 255, 255),
    (255, 51, 102),
    (255, 255, 255),
    (153, 153, 153),
    (128, 128, 128),
];

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
#[repr(u8)]
pub enum DecColor {
    C0,
    C1,
    C2,
    C3,
    C4,
    C5,
    C6,
    C7,
    C8,
    C9,
}

impl TryFrom<u8> for DecColor {
    type Error = ColorError;

    fn try_from(val: u8) -> Result<Self, Self::Error> {
        match val {
            0 => Ok(DecColor::C0),
            1 => Ok(DecColor::C1),
            2 => Ok(DecColor::C2),
            3 => Ok(DecColor::C3),
            4 => Ok(DecColor::C4),
            5 => Ok(DecColor::C5),
            6 => Ok(DecColor::C6),
            7 => Ok(DecColor::C7),
            8 => Ok(DecColor::C8),
            9 => Ok(DecColor::C9),
            _ => Err(ColorError::InvalidDecColor(val)),
        }
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
#[repr(u8)]
pub enum HexColor {
    H0,
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
    H7,
    H8,
    H9,
    HA,
    HB,
    HC,
    HD,
    HE,
    HF,
}

impl TryFrom<u8> for HexColor {
    type Error = ColorError;

    fn try_from(val: u8) -> Result<Self, Self::Error> {
        match val {
            0 => Ok(HexColor::H0),
            1 => Ok(HexColor::H1),
            2 => Ok(HexColor::H2),
            3 => Ok(HexColor::H3),
            4 => Ok(HexColor::H4),
            5 => Ok(HexColor::H5),
            6 => Ok(HexColor::H6),
            7 => Ok(HexColor::H7),
            8 => Ok(HexColor::H8),
            9 => Ok(HexColor::H9),
            10 => Ok(HexColor::HA),
            11 => Ok(HexColor::HB),
            12 => Ok(HexColor::HC),
            13 => Ok(HexColor::HD),
            14 => Ok(HexColor::HE),
            15 => Ok(HexColor::HF),
            _ => Err(ColorError::InvalidHexColor(val)),
        }
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum DPColor {
    DecColor(DecColor),
    RGBColor(HexColor, HexColor, HexColor),
}

impl DPColor {
    pub fn from_dec_color(c: &str) -> Result<Self, ColorError> {
        let ix: u8 = c.parse()?;
        let dc = DecColor::try_from(ix)?;
        Ok(DPColor::DecColor(dc))
    }

    pub fn from_hex_color(input: &str) -> Result<Self, ColorError> {
        let mut rgb: [u8; 3] = [0, 0, 0];
        let mut s = String::with_capacity(1);
        for (ix, i) in input.chars().take(3).enumerate() {
            s.clear();
            s.push(i);
            rgb[ix] = u8::from_str_radix(&s, 16)?;
        }
        Ok(DPColor::RGBColor(
            HexColor::try_from(rgb[0])?,
            HexColor::try_from(rgb[1])?,
            HexColor::try_from(rgb[2])?,
        ))
    }

    pub fn to_rgb(self) -> RGBColor {
        match self {
            DPColor::DecColor(d) => RGBColor::from_u8_tuple(DEC_COLORS[d as usize]),
            DPColor::RGBColor(r, g, b) => {
                RGBColor::from_u8_tuple(((r as u8) * 17, (g as u8) * 17, (b as u8) * 17))
            }
        }
    }

    pub fn to_dp_color(self) -> String {
        match self {
            DPColor::DecColor(d) => format!("^{}", d as u8),
            DPColor::RGBColor(r, g, b) => format!("^x{:X}{:X}{:X}", r as u8, g as u8, b as u8),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct RGBColor {
    pub r: f64,
    pub g: f64,
    pub b: f64,
}

impl Hash for RGBColor {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.int_r().hash(state);
        self.int_g().hash(state);
        self.int_b().hash(state);
    }
}

impl PartialEq for RGBColor {
    fn eq(&self, other: &Self) -> bool {
        (self.int_r() == other.int_r())
            && (self.int_g() == other.int_g())
            && (self.int_b() == other.int_b())
    }
}

impl Eq for RGBColor {}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct HSLColor {
    pub h: f64,
    pub s: f64,
    pub l: f64,
}

impl RGBColor {
    fn from_u8_tuple(input: (u8, u8, u8)) -> Self {
        Self {
            r: (input.0 as f64) / 255.,
            g: (input.1 as f64) / 255.,
            b: (input.2 as f64) / 255.,
        }
    }

    pub fn int_r(self) -> u8 {
        (self.r * 255.0).round() as u8
    }

    pub fn int_g(self) -> u8 {
        (self.g * 255.0).round() as u8
    }

    pub fn int_b(self) -> u8 {
        (self.b * 255.0).round() as u8
    }

    pub fn to_hsl(self) -> HSLColor {
        let eps = 0.000_000_001;
        let maxc = f64::max(self.r, f64::max(self.g, self.b));
        let minc = f64::min(self.r, f64::min(self.g, self.b));
        let l = (maxc + minc) / 2.;
        if (minc - maxc).abs() < eps {
            HSLColor { h: 0., s: 0., l }
        } else {
            let s = if l < 0.5 + eps {
                (maxc - minc) / (maxc + minc)
            } else {
                (maxc - minc) / (2.0 - maxc - minc)
            };
            let rc = (maxc - self.r) / (maxc - minc);
            let gc = (maxc - self.g) / (maxc - minc);
            let bc = (maxc - self.b) / (maxc - minc);
            let mut h = if (self.r - maxc).abs() < eps {
                bc - gc
            } else if (self.g - maxc).abs() < eps {
                2. + rc - bc
            } else {
                4. + gc - rc
            };
            h = (h / 6.0) % 1.0;
            HSLColor { h, s, l }
        }
    }
}

impl HSLColor {
    #[cfg(test)]
    fn new(h: f64, s: f64, l: f64) -> Self {
        Self { h, s, l }
    }
    fn _v(m1: f64, m2: f64, hue: f64) -> f64 {
        let hue = if hue < 0. { (hue + 1.) % 1. } else { hue % 1. };
        if hue < 1. / 6. {
            return m1 + (m2 - m1) * hue * 6.0;
        }

        if hue < 0.5 {
            return m2;
        }
        if hue < 2. / 3. {
            return m1 + (m2 - m1) * (2. / 3. - hue) * 6.0;
        }
        m1
    }
    pub fn to_rgb(self) -> RGBColor {
        let eps = 0.000_000_001;
        if self.s.abs() < eps {
            RGBColor {
                r: self.l,
                g: self.l,
                b: self.l,
            }
        } else {
            let m2 = if self.l < 0.5 + eps {
                self.l * (1.0 + self.s)
            } else {
                self.l + self.s - (self.l * self.s)
            };
            let m1 = 2.0 * self.l - m2;
            RGBColor {
                r: Self::_v(m1, m2, self.h + 1. / 3.),
                g: Self::_v(m1, m2, self.h),
                b: Self::_v(m1, m2, self.h - 1. / 3.),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{HSLColor, RGBColor};

    #[test]
    fn to_hsl() {
        let c = RGBColor::from_u8_tuple((0, 0, 0));
        let hc = c.to_hsl();
        assert_eq!(hc.h, 0.);
        assert_eq!(hc.s, 0.);
        assert_eq!(hc.l, 0.);
        let c = RGBColor::from_u8_tuple((100, 100, 100));
        let hc = c.to_hsl();
        assert_eq!(hc.h, 0.);
        assert_eq!(hc.s, 0.);
        assert_eq!(hc.l, 0.39215686274509803);
        let c = RGBColor::from_u8_tuple((128, 64, 64));
        let hc = c.to_hsl();
        assert_eq!(hc.h, 0.);
        assert_eq!(hc.s, 0.3333333333333333);
        assert_eq!(hc.l, 0.3764705882352941);
        let c = RGBColor::from_u8_tuple((64, 64, 128));
        let hc = c.to_hsl();
        assert_eq!(hc.h, 0.6666666666666666,);
        assert_eq!(hc.s, 0.3333333333333333);
        assert_eq!(hc.l, 0.3764705882352941);
        let c = RGBColor::from_u8_tuple((85, 17, 153));
        let hc = c.to_hsl();
        assert_eq!(hc.h, 0.75,);
        assert_eq!(hc.s, 0.8);
        assert_eq!(hc.l, 0.3333333333333333,);
    }

    #[test]
    fn to_rgb() {
        let c = HSLColor::new(0., 0., 0.);
        let rc = c.to_rgb();
        assert_eq!(rc.r, 0.);
        assert_eq!(rc.g, 0.);
        assert_eq!(rc.b, 0.);
        let c = HSLColor::new(0., 0., 0.39215686274509803);
        let rc = c.to_rgb();
        assert_eq!(rc.r, 0.39215686274509803);
        assert_eq!(rc.g, 0.39215686274509803);
        assert_eq!(rc.b, 0.39215686274509803);
        let c = HSLColor::new(0., 0.3333333333333333, 0.3764705882352941);
        let rc = c.to_rgb();
        assert_eq!(rc.r, 0.5019607843137255);
        assert_eq!(rc.g, 0.25098039215686274);
        assert_eq!(rc.b, 0.25098039215686274);
        let c = HSLColor::new(0.6666666666666666, 0.3333333333333333, 0.3764705882352941);
        let rc = c.to_rgb();
        assert_eq!(rc.r, 0.25098039215686274);
        assert_eq!(rc.g, 0.25098039215686274);
        assert_eq!(rc.b, 0.5019607843137255);
        let c = HSLColor::new(0.75, 0.8, 0.3333333333333333);
        let rc = c.to_rgb();
        assert_eq!(rc.r, 0.3333333333333331);
        assert_eq!(rc.g, 0.06666666666666665);
        assert_eq!(rc.b, 0.6);
    }
}
