mod colors;
mod constants;
mod parse;
mod to_ansi;
mod to_html;
mod to_irc;

use self::colors::DPColor;
use self::parse::dp_string;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct DPColoredPart {
    color: DPColor,
    text: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct DPString {
    head: String,
    parts: Vec<DPColoredPart>,
}

fn split_str(s: &str, pos: usize) -> (String, String) {
    let mut s1 = String::with_capacity(pos);
    let mut s2 = String::with_capacity(s.chars().count() - pos);
    for (ix, c) in s.chars().enumerate() {
        if ix < pos {
            s1.push(c)
        } else {
            s2.push(c)
        }
    }
    (s1, s2)
}

impl DPString {
    pub fn starts_with(&self, s: &str) -> bool {
        self.to_none().starts_with(s)
    }

    pub fn is_empty(&self) -> bool {
        self.to_none().is_empty()
    }

    pub fn skip(&self, count: usize) -> Self {
        let (_, res) = self.split_at(count);
        res
    }

    pub fn split_at(&self, pos: usize) -> (Self, Self) {
        // this may actually panic gotta be careful when calling it...
        if pos <= self.head.chars().count() {
            let (h1, h2) = split_str(&self.head, pos);
            (
                Self {
                    head: h1,
                    parts: Vec::new(),
                },
                Self {
                    head: h2,
                    parts: self.parts.clone(),
                },
            )
        } else {
            let mut cur = self.head.chars().count();
            let mut cur_part = 0;
            let mut parts1 = Vec::new();
            let mut parts2 = Vec::new();
            while cur < pos {
                cur += self.parts[cur_part].text.chars().count();
                parts1.push(self.parts[cur_part].clone());
                cur_part += 1;
            }
            let split_last_part_at = pos - (cur - parts1[cur_part - 1].text.chars().count());
            let (text1, text2) = split_str(&parts1[cur_part - 1].text, split_last_part_at);
            parts1[cur_part - 1].text = text1;
            if !text2.is_empty() {
                parts2.push(DPColoredPart {
                    color: parts1[cur_part - 1].color,
                    text: text2,
                });
            }
            while cur_part < self.parts.len() {
                parts2.push(self.parts[cur_part].clone());
                cur_part += 1;
            }
            (
                Self {
                    head: String::from(&self.head),
                    parts: parts1,
                },
                Self {
                    head: String::from(""),
                    parts: parts2,
                },
            )
        }
    }

    pub fn split_once(self, pat: char) -> Option<(Self, Self)> {
        if let Some((ix, _)) = self.to_none().chars().enumerate().find(|(_, c)| *c == pat) {
            let (s1, s2) = self.split_at(ix);
            Some((s1, s2.skip(1)))
        } else {
            None
        }
    }

    pub fn parse(source: &str) -> Self {
        match dp_string(source) {
            Ok((_, s)) => s,
            Err(e) => {
                println!("{:?}", e);
                // All valid unicode strings should parse as dpstring
                // if not it's a bug in code, and panic will tell it to us LOUDLY
                panic!("Failed to parse {}", source)
            }
        }
    }

    pub fn parse_irc(source: &str) -> Self {
        // just a stub for now...
        Self {
            head: String::from(source),
            parts: Vec::new(),
        }
    }

    pub fn parse_none(source: &str) -> Self {
        Self {
            head: String::from(source),
            parts: Vec::new(),
        }
    }

    pub fn to_dp(&self) -> String {
        let mut res = self.head.replace('^', "^^");
        for i in self.parts.iter() {
            res.push_str(&i.color.to_dp_color());
            res.push_str(&i.text.replace('^', "^^"));
        }
        res
    }

    pub fn to_none(&self) -> String {
        let mut res = self.head.clone();
        for i in self.parts.iter() {
            res.push_str(&i.text);
        }
        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_convert() {
        let rs = "^xFB0✙МOROSOΦOS^7";

        let s = DPString::parse(rs);
        assert_eq!(s.to_dp(), rs);
        let dps = DPString::parse("world ^☹");
        assert_eq!(dps.head, "world ^☹");
        let dps = DPString::parse("^9foobar^7");
        assert_eq!(dps.to_dp(), "^9foobar^7");
        // let dps = DPString::parse(
        //     "^x0bf5^xf892^xfff2^xeee▮^xddd▮^xccc▮^xbbb▮^xaaa:^x999h^x888a^x777p^x666p^x555y^x444^7",
        // );
        // assert_eq!(
        //     dps.to_dp(),
        //     "^x0bf5^xf892^xfff2^xeee▮^xddd▮^xccc▮^xbbb▮^xaaa:^x999h^x888a^x777p^x666p^x555y^x444^7"
        // );
    }

    #[test]
    fn test_split() {
        let s = DPString::parse("");
        assert!(s.split_once(' ').is_none());
        let s = DPString::parse("ᄠ뱠 ");
        let (s1, s2) = s.split_once(' ').unwrap();
        assert_eq!(s1, DPString::parse("ᄠ뱠"));
        assert_eq!(s2, DPString::parse(""));
        let s = DPString::parse("hello world");
        let (s1, s2) = s.split_once(' ').unwrap();
        assert_eq!(s1, DPString::parse("hello"));
        assert_eq!(s2, DPString::parse("world"));
        let s = DPString::parse("^2hello ^3world");
        let (s1, s2) = s.split_once(' ').unwrap();
        assert_eq!(s1, DPString::parse("^2hello"));
        assert_eq!(s2, DPString::parse("^3world"));
        let s = DPString::parse("^1test^2something^3complex");
        let (s1, s2) = s.split_at(6);
        assert_eq!(s1, DPString::parse("^1test^2so"));
        assert_eq!(s2, DPString::parse("^2mething^3complex"));
    }
}
