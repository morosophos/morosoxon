use self::constants::*;

use super::colors::DPColor;
use super::*;
use nom::branch::alt;
use nom::bytes::complete::{tag, take_while_m_n};
use nom::character::complete::none_of;
use nom::combinator::{map, not, peek};
use nom::multi::{fold_many0, many0};
use nom::sequence::{preceded, tuple};
use nom::IResult;

fn is_hex_digit(c: char) -> bool {
    c.is_digit(16)
}

fn is_digit(c: char) -> bool {
    c.is_digit(10)
}

// decimal color code ^0..^9
fn dec_color_code(inp: &str) -> IResult<&str, &str> {
    take_while_m_n(1, 1, is_digit)(inp)
}

// hex RGB color code, eg ^x2a6
fn hex_color_code(inp: &str) -> IResult<&str, &str> {
    preceded(tag("x"), take_while_m_n(3, 3, is_hex_digit))(inp)
}

// A caret which isn't followed by either dec or hex color code
fn stray_caret(inp: &str) -> IResult<&str, char> {
    map(
        preceded(tag("^"), peek(not(alt((dec_color_code, hex_color_code))))),
        |_| '^',
    )(inp)
}

// a caret can be quoted by other caret, i.e. ^^ reads as ^
fn quoted_caret(inp: &str) -> IResult<&str, char> {
    map(tag("^^"), |_| '^')(inp)
}

// Apply qfont character transform to characters
fn character(inp: &str) -> IResult<&str, char> {
    map(none_of("^"), dp_transform_char)(inp)
}

fn contents_piece(inp: &str) -> IResult<&str, char> {
    alt((quoted_caret, stray_caret, character))(inp)
}

fn contents(inp: &str) -> IResult<&str, String> {
    fold_many0(contents_piece, String::new, |mut acc: String, item| {
        acc.push(item);
        acc
    })(inp)
}

fn dec_color(inp: &str) -> IResult<&str, DPColor> {
    map(preceded(tag("^"), dec_color_code), |d| {
        DPColor::from_dec_color(d).unwrap()
    })(inp)
}

fn hex_color(inp: &str) -> IResult<&str, DPColor> {
    map(preceded(tag("^"), hex_color_code), |h| {
        DPColor::from_hex_color(h).unwrap()
    })(inp)
}

fn color(inp: &str) -> IResult<&str, DPColor> {
    alt((hex_color, dec_color))(inp)
}

fn dp_part(inp: &str) -> IResult<&str, DPColoredPart> {
    map(tuple((color, contents)), |(c, cnt)| DPColoredPart {
        color: c,
        text: cnt,
    })(inp)
}

pub fn dp_string(inp: &str) -> IResult<&str, DPString> {
    map(tuple((contents, many0(dp_part))), |(head, parts)| {
        DPString { head, parts }
    })(inp)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dp_string::colors::DecColor;

    #[test]
    fn test_basic() {
        let s = "^1Hello".to_string();
        let dps = DPString::parse(&s);
        assert_eq!(dps.head, "");
        assert_eq!(dps.parts[0].text, "Hello");
        assert_eq!(dps.parts[0].color, DPColor::DecColor(DecColor::C1));
    }

    #[test]
    fn test_stray_caret() {
        let dps = DPString::parse("^nasty");
        assert_eq!(dps.head, "^nasty");
        let dps = DPString::parse("nasty^");
        assert_eq!(dps.head, "nasty^");
    }

    #[test]
    fn test_unicode() {
        let dps = DPString::parse("привет, καλημέρα");
        assert_eq!(dps.head, "привет, καλημέρα")
    }

    #[test]
    fn test_unicode_after_caret() {
        let dps = DPString::parse("hello ^п");
        assert_eq!(dps.head, "hello ^п");
        let dps = DPString::parse("world ^xппп");
        assert_eq!(dps.head, "world ^xппп");
    }

    #[test]
    fn test_unicode_transform() {
        let dps = DPString::parse("hello\u{e008}unicode");
        assert_eq!(&dps.head, "hello\u{01F52B}unicode")
    }
}
