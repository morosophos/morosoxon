use super::{DPColoredPart, DPString};

use ansi_term::{Color, Style};

impl DPColoredPart {
    fn to_ansi_24bit(&self) -> String {
        let rgb_color = self.color.to_rgb();
        let c = Color::RGB(rgb_color.int_r(), rgb_color.int_g(), rgb_color.int_b());
        c.paint(self.text.as_str()).to_string()
    }
}

impl DPString {
    pub fn to_ansi_24bit(&self) -> String {
        let mut parts: Vec<String> = Vec::new();
        let h = Style::default().paint(self.head.as_str()).to_string();
        parts.push(h);
        for colored_part in self.parts.iter() {
            parts.push(colored_part.to_ansi_24bit());
        }
        parts.concat()
    }
}

#[cfg(test)]
mod tests {
    use super::DPString;

    #[test]
    fn to_ansi_24bit() {
        let dp = DPString::parse("hello");
        assert_eq!(dp.to_ansi_24bit(), "hello");
        let dp = DPString::parse("^1red^x125hex");
        assert_eq!(
            dp.to_ansi_24bit(),
            "\u{1b}[38;2;255;0;0mred\u{1b}[0m\u{1b}[38;2;17;34;85mhex\u{1b}[0m"
        );
    }
}
