use super::colors::RGBColor;
use super::{DPColoredPart, DPString};
use unicode_segmentation::UnicodeSegmentation;
use v_htmlescape::escape;

const CONTRAST_THRESHOLD: f64 = 0.20;

fn process_color(c: &RGBColor) -> (u8, u8, u8) {
    let mut hsl = c.to_hsl();
    if hsl.l < CONTRAST_THRESHOLD {
        hsl.l = CONTRAST_THRESHOLD;
    }
    let new_rgb: RGBColor = hsl.to_rgb();
    (new_rgb.int_r(), new_rgb.int_g(), new_rgb.int_b())
}

impl DPColoredPart {
    fn to_html(&self) -> String {
        let (r, g, b) = process_color(&self.color.to_rgb());
        format!(
            "<span style=\"color:rgb({},{},{})\">{}</span>",
            r,
            g,
            b,
            escape(&self.text)
        )
    }
}

const ELLIPSIS: &str = "...";

fn truncate(s: &str, new_len: usize) -> (String, usize) {
    let mut res = String::new();
    let mut cur_len = 0;
    for i in UnicodeSegmentation::graphemes(s, true) {
        if cur_len >= new_len {
            res.push_str(ELLIPSIS);
            break;
        }
        res.push_str(i);
        cur_len += 1;
    }
    (res, cur_len)
}

impl DPString {
    pub fn to_html(&self) -> String {
        let mut res = escape(&self.head).to_string();
        for i in self.parts.iter() {
            res.push_str(&i.to_html());
        }
        res
    }

    pub fn to_html_truncated(&self, maxchars: usize) -> String {
        let (mut res, mut current_len) = truncate(&self.head, maxchars);
        let mut parts = self.parts.iter();
        let mut next_part = parts.next();
        while current_len < maxchars {
            if let Some(p) = next_part {
                let available_len = maxchars - current_len;
                let (text, part_len) = truncate(&p.text, available_len);
                let (r, g, b) = process_color(&p.color.to_rgb());
                let part_text = format!(
                    "<span style=\"color:rgb({},{},{})\">{}</span>",
                    r,
                    g,
                    b,
                    escape(&text)
                );
                res.push_str(&part_text);
                current_len += part_len;
            } else {
                break;
            }
            next_part = parts.next();
        }
        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_html() {
        let s = "^x600M^x500a^x400k^x300m^x200a^x100k^7";
        let d = DPString::parse(s);
        assert_eq!(d.to_html(), "<span style=\"color:rgb(102,0,0)\">M</span><span style=\"color:rgb(102,0,0)\">a</span><span style=\"color:rgb(102,0,0)\">k</span><span style=\"color:rgb(102,0,0)\">m</span><span style=\"color:rgb(102,0,0)\">a</span><span style=\"color:rgb(102,0,0)\">k</span><span style=\"color:rgb(255,255,255)\"></span>");
    }
}
