use super::colors::RGBColor;
use super::{DPColoredPart, DPString};

#[derive(Debug, Clone, Copy)]
enum Luma {
    Dark,
    Light,
}

#[derive(Debug, Clone, Copy)]
enum Chroma {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
}

fn rgb_to_irc(c: RGBColor) -> (Luma, Chroma) {
    let hsl = c.to_hsl();
    let v = hsl.l + hsl.s * f64::min(hsl.l, 1. - hsl.l);
    let s = if v < f64::EPSILON {
        0.
    } else {
        2. * (1. - hsl.l)
    };
    let luma = if v > 0.6 { Luma::Light } else { Luma::Dark };
    if s > 0.3 {
        let step = 1.0 / 6.0;
        let chroma = if hsl.h < step / 2. {
            Chroma::Red
        } else if hsl.h < step / 2. + step {
            Chroma::Yellow
        } else if hsl.h < step / 2. + 2. * step {
            Chroma::Green
        } else if hsl.h < step / 2. + 3. * step {
            Chroma::Cyan
        } else if hsl.h < step / 2. + 4. * step {
            Chroma::Blue
        } else if hsl.h < step / 2. + 5. * step {
            Chroma::Magenta
        } else {
            Chroma::Red
        };
        (luma, chroma)
    } else if v < 0.45 {
        (luma, Chroma::Black)
    } else {
        (luma, Chroma::White)
    }
}

fn get_irc_color_code(l: Luma, c: Chroma) -> &'static str {
    match l {
        Luma::Light => match c {
            Chroma::Black => "\x0314",
            Chroma::Red => "\x0304",
            Chroma::Green => "\x0309",
            Chroma::Yellow => "\x0308",
            Chroma::Blue => "\x0312",
            Chroma::Magenta => "\x0313",
            Chroma::Cyan => "\x0311",
            Chroma::White => "\x0f",
        },
        Luma::Dark => match c {
            Chroma::Black => "\x0301",
            Chroma::Red => "\x0305",
            Chroma::Green => "\x0303",
            Chroma::Yellow => "\x0307",
            Chroma::Blue => "\x0302",
            Chroma::Magenta => "\x0306",
            Chroma::Cyan => "\x0310",
            Chroma::White => "\x0315",
        },
    }
}

impl DPColoredPart {
    fn to_irc(&self) -> String {
        let (l, c) = rgb_to_irc(self.color.to_rgb());
        format!("{}{}", get_irc_color_code(l, c), self.text)
    }
}

impl DPString {
    pub fn to_irc(&self) -> String {
        let mut res = self.head.clone();
        for i in self.parts.iter() {
            res.push_str(&i.to_irc());
        }
        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_irc() {
        let dpstr = DPString::parse("^1he^2llo");
        assert_eq!(dpstr.to_irc(), "\x0304he\x0309llo");
        let dpstr = DPString::parse("^1he^2llo^7world");
        assert_eq!(dpstr.to_irc(), "\x0304he\x0309llo\x0fworld");
    }
}
