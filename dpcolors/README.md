# dpcolors

Dpcolors is a library to convert strings with DP color codes and quakefont characters into other formats, namely:

 * plain strings (color codes stripped)
 * ANSI color codes
 * IRC color codes
 * HTML
 
quakefont characters are converted to their unicode counterparts

## Example

```
use dpcolors::DPString;
   
let dp_string = DPString::parse("^1this text should be red");
println!("Stripped colors: {}", dp_string.to_none());
println!("ANSI colors: {}", dp_string.to_ansi_24bit());
println!("HTML full: {}", dp_string.to_html());
println!("HTML truncated: {}", dp_string.to_html_truncated(25));
```
