#![no_main]

use dpcolors::DPString;
use libfuzzer_sys::fuzz_target;

fuzz_target!(|data: &str| {
    let dp_string = DPString::parse(data);
    dp_string.to_none();
    dp_string.to_ansi_24bit();
    dp_string.to_html();
    dp_string.to_html_truncated(25);
    dp_string.split_once(' ');
});
