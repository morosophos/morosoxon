mod rcon;
mod timestamp;

use tokio::time::sleep;

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    let remote = rcon::RconRemote::new(
        "127.0.0.1:26000",
        rcon::RconSecure::from_u8(1)?,
        String::from("12345"),
    );
    let (connection, _recv) = rcon::RconConnection::connect(remote).await.unwrap();
    println!("Connected...");
    loop {
        // connection.send("g_maplist").await.unwrap();
        // task::sleep(std::time::Duration::from_secs(1)).await;
        connection.send("status 1").await.unwrap();
        // task::sleep(std::time::Duration::from_secs(1)).await;
        sleep(std::time::Duration::from_secs(10)).await;
    }
}
