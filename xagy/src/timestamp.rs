use std::time::{Duration, SystemTime, UNIX_EPOCH};

pub struct Timestamp {
    pub secs: u64,
    pub subsecs: f64,
}

impl Timestamp {
    pub fn new(time_diff: u64) -> Self {
        let t = (SystemTime::now() + Duration::from_secs(time_diff))
            .duration_since(UNIX_EPOCH)
            .unwrap();
        Self {
            secs: t.as_secs(),
            subsecs: f64::from(t.subsec_nanos()) * 0.001 * 0.001 * 0.001,
        }
    }
}
