mod base_parser;
mod cmd_parser;
mod types;

use crate::timestamp::Timestamp;
use bytes::{BufMut, BytesMut};
use hmac::{Hmac, Mac};
use md4::Md4;
use std::collections::VecDeque;
use std::net::SocketAddr;
use std::{sync::Arc, time::Duration};
use thiserror::Error;
use tokio::net::UdpSocket;
use tokio::sync::mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender};
use tokio::time::sleep;

use base_parser::{CombinedParser, RconDataParser};
use cmd_parser::get_cmd_parser;
pub use cmd_parser::RconCmdItem;

const MAX_PACKET_SIZE: usize = 1400;
const QUAKE_PACKET_HEADER: &[u8] = &[b'\xFF'; 4];
const RCON_RESPONSE_HEADER: &[u8] = b"\xFF\xFF\xFF\xFFn";
// const CHALLENGE_PACKET: &[u8] = b"\xFF\xFF\xFF\xFFgetchallenge";
// const CHALLENGE_RESPONSE_HEADER: &[u8] = b"\xFF\xFF\xFF\xFFchallenge ";

type HmacMd4 = Hmac<Md4>;

fn hmac_md4(key: &[u8], msg: &[u8]) -> Vec<u8> {
    let mut mac = HmacMd4::new_from_slice(key).unwrap();
    mac.update(msg);
    let code = mac.finalize().into_bytes();
    code.to_vec()
}

#[derive(Error, Debug)]
pub enum RconError {
    #[error("io error")]
    IOError(#[from] std::io::Error),
    #[error("Invalid secure value")]
    InvalidSecureValue(u8),
}

#[derive(PartialEq, Clone, Copy, Debug)]
pub enum RconSecure {
    None,
    Time,
    // Let's not support challenge atm, too much hassle
    // Challenge,
}

impl RconSecure {
    pub fn from_u8(val: u8) -> Result<Self, RconError> {
        match val {
            0 => Ok(RconSecure::None),
            1 => Ok(RconSecure::Time),
            // 2 => Ok(RconSecure::Challenge),
            _ => Err(RconError::InvalidSecureValue(val)),
        }
    }
}

#[derive(Clone, Debug)]
pub struct RconRemote {
    pub addr: SocketAddr,
    pub secure: RconSecure,
    pub password: String,
}

impl RconRemote {
    pub fn new(address: &str, secure: RconSecure, password: String) -> Self {
        Self {
            addr: address.parse().unwrap(),
            secure,
            password,
        }
    }
}

struct RconOutgoingPacket<'a> {
    secure: RconSecure,
    command: &'a str,
    password: &'a str,
}

impl<'a> RconOutgoingPacket<'a> {
    fn nosecure_packet(&self) -> Vec<u8> {
        [
            QUAKE_PACKET_HEADER,
            self.password.as_bytes(),
            b" ",
            self.command.as_bytes(),
        ]
        .concat()
    }

    fn secure_time_packet(&self, time_diff: u64) -> Vec<u8> {
        let t = Timestamp::new(time_diff);

        let cmd = format!("{}.{:.6} {}", t.secs, t.subsecs, self.command).into_bytes();
        let key = hmac_md4(self.password.as_bytes(), &cmd);
        [
            QUAKE_PACKET_HEADER,
            b"srcon HMAC-MD4 TIME ",
            &key,
            b" ",
            &cmd,
        ]
        .concat()
    }
    pub fn prepare(&'a self) -> Vec<u8> {
        match self.secure {
            RconSecure::None => self.nosecure_packet(),
            RconSecure::Time => self.secure_time_packet(0),
        }
    }
}

struct RconCmdReceiver {
    socket: Arc<UdpSocket>,
    item_sender: UnboundedSender<RconCmdItem>,
    parser: CombinedParser<RconCmdItem>,
    lines: VecDeque<String>,
    buffer: BytesMut,
}

impl RconCmdReceiver {
    fn new(socket: Arc<UdpSocket>, item_sender: UnboundedSender<RconCmdItem>) -> Self {
        Self {
            socket,
            item_sender,
            parser: get_cmd_parser(),
            lines: VecDeque::new(),
            buffer: BytesMut::with_capacity(MAX_PACKET_SIZE * 5),
        }
    }

    fn process_buffer(&mut self) {
        loop {
            let maybe_pos = self
                .buffer
                .iter()
                .enumerate()
                .find(|&(_, c)| *c == b'\n')
                .map(|(i, _)| i);
            if let Some(pos) = maybe_pos {
                let mut line = self.buffer.split_to(pos + 1);
                line.truncate(pos);
                let s = String::from_utf8_lossy(&line[..]).to_owned().to_string();
                self.lines.push_back(s);
            } else {
                break;
            }
        }
    }

    async fn receive_data(&mut self) -> Result<(), RconError> {
        loop {
            let mut buf = [0u8; MAX_PACKET_SIZE];
            let n = self.socket.recv(&mut buf).await?;
            let header_len = RCON_RESPONSE_HEADER.len();
            if self.buffer.remaining_mut() < n - header_len {
                self.buffer.reserve(n - header_len);
            }
            self.buffer.put(&buf[header_len..n]);
            if &buf[0..header_len] != RCON_RESPONSE_HEADER {
                continue;
            }
            self.process_buffer();
            if let Some(data) = self.parser.parse(&mut self.lines) {
                for i in data.into_iter() {
                    self.item_sender.send(i).unwrap();
                }
            }

            sleep(Duration::from_millis(100)).await;
        }
    }
}

pub struct RconConnection {
    socket: Arc<UdpSocket>,
    remote: RconRemote,
}

impl RconConnection {
    pub async fn connect(
        remote: RconRemote,
    ) -> Result<(Self, UnboundedReceiver<RconCmdItem>), RconError> {
        let socket = UdpSocket::bind("0.0.0.0:0").await?;
        socket.connect(&remote.addr).await?;
        let socket_arc = Arc::new(socket);
        let connection = Self {
            socket: socket_arc.clone(),
            remote,
        };
        let (sender, receiver) = unbounded_channel();
        let mut cmd_receiver = RconCmdReceiver::new(socket_arc, sender);
        tokio::spawn(async move { cmd_receiver.receive_data().await });
        Ok((connection, receiver))
    }

    pub async fn send(&self, data: &str) -> Result<(), RconError> {
        let packet = RconOutgoingPacket {
            secure: self.remote.secure,
            command: data,
            password: &self.remote.password,
        };
        let bytes = packet.prepare();
        println!("Sending {:?}", bytes);
        self.socket.send(&bytes).await?;
        Ok(())
    }
}
