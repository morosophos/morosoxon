use lazy_static::lazy_static;
use regex::Regex;
use std::collections::VecDeque;
use std::net::IpAddr;

use crate::rcon::base_parser::{CombinedParser, RconDataParser};
use crate::rcon::types::*;

#[derive(Debug)]
pub enum RconCmdItem {
    StatusHost(String),
    StatusVersion(String),
    StatusProtocol(u32, String),
    StatusMap(MapName),
    StatusTiming {
        cpu: f32,
        lost: f32,
        offset_avg: f32,
        offset_max: f32,
        offset_sdev: f32,
    },
    StatusPlayers(u8, u8),
    StatusPlayer {
        entity_id: EntityId,
        ip: IpAddr,
        port: u32,
        ping: i32,
        frags: i32,
        time: u32,
        pl: u32,
        name: String,
    },
    Cvar(CvarName, CvarValue),
}

struct CvarParser {}

impl RconDataParser for CvarParser {
    type Item = RconCmdItem;
    fn parse(&mut self, lines: &mut VecDeque<String>) -> Option<Vec<RconCmdItem>> {
        lazy_static! {
            static ref RE: Regex =
                Regex::new(r#"^(?:")?(?:\^3)?(\w+)(?:\^7)?(?:")? is "([^"]*)""#).unwrap();
        }
        let mut res = Vec::new();
        if let Some(line) = lines.front() {
            if let Some(caps) = RE.captures(line) {
                let name = String::from(caps.get(1).unwrap().as_str());
                let value = String::from(caps.get(2).unwrap().as_str());
                res.push(RconCmdItem::Cvar(name, value))
            }
        }
        if !res.is_empty() {
            lines.pop_front();
        }
        Some(res)
    }
}

struct StatusLineParser {}

impl RconDataParser for StatusLineParser {
    type Item = RconCmdItem;

    fn parse(&mut self, lines: &mut VecDeque<String>) -> Option<Vec<RconCmdItem>> {
        lazy_static! {
            static ref RE: Regex =
                Regex::new(r#"^(host|version|protocol|map|timing|players):\s*(.*)$"#).unwrap();
            static ref PROTOCOL_RE: Regex = Regex::new(r#"(\d+) \((.*)\)"#).unwrap();
            static ref PLAYERS_RE: Regex = Regex::new(r#"(\d+) active \((\d+) max\)"#).unwrap();
            static ref TIMING_RE: Regex = Regex::new(
                // 37.9% CPU, 0.00% lost, offset avg 2.4ms, max 13.3ms, sdev 2.6ms
                r#"(\d{1,2}\.\d)% CPU, (\d+\.\d{2})% lost, offset avg (\d+\.\d)ms, max (\d+\.\d)ms, sdev (\d+\.\d)ms"#
            ).unwrap();
        }
        let mut res = Vec::new();
        if let Some(line) = lines.front() {
            if let Some(caps) = RE.captures(line) {
                use RconCmdItem::*;
                let status_type = caps.get(1).unwrap().as_str();
                let status_value = caps.get(2).unwrap().as_str();
                let maybe_item = match status_type {
                    "host" => Some(StatusHost(String::from(status_value))),
                    "version" => Some(StatusVersion(String::from(status_value))),
                    "protocol" => {
                        if let Some(caps) = PROTOCOL_RE.captures(status_value) {
                            Some(StatusProtocol(
                                caps.get(1).unwrap().as_str().parse().unwrap(),
                                String::from(caps.get(2).unwrap().as_str()),
                            ))
                        } else {
                            None
                        }
                    }
                    "map" => Some(StatusMap(String::from(status_value))),
                    "timing" => {
                        if let Some(caps) = TIMING_RE.captures(status_value) {
                            Some(StatusTiming {
                                cpu: caps.get(1).unwrap().as_str().parse().unwrap(),
                                lost: caps.get(2).unwrap().as_str().parse().unwrap(),
                                offset_avg: caps.get(3).unwrap().as_str().parse().unwrap(),
                                offset_max: caps.get(4).unwrap().as_str().parse().unwrap(),
                                offset_sdev: caps.get(5).unwrap().as_str().parse().unwrap(),
                            })
                        } else {
                            None
                        }
                    }
                    "players" => {
                        if let Some(caps) = PLAYERS_RE.captures(status_value) {
                            Some(StatusPlayers(
                                caps.get(1).unwrap().as_str().parse().unwrap(),
                                caps.get(2).unwrap().as_str().parse().unwrap(),
                            ))
                        } else {
                            None
                        }
                    }
                    _ => None,
                };
                if let Some(item) = maybe_item {
                    res.push(item);
                }
            }
        }
        if !res.is_empty() {
            lines.pop_front();
        }
        Some(res)
    }
}

struct StatusPlayerParser {}

impl RconDataParser for StatusPlayerParser {
    type Item = RconCmdItem;

    fn parse(&mut self, lines: &mut VecDeque<String>) -> Option<Vec<RconCmdItem>> {
        // print ("%s%-47s %2i %4i %2i:%02i:%02i %4i  #%-3u ^7%s\n", k%2 ? "^3" : "^7", ip, packetloss, ping, hours, minutes, seconds, frags, i+1, client->name);
        lazy_static! {
            static ref RE: Regex = Regex::new(
                r#"^\^(?:3|7)\[?(?P<ip>[\d:\.]+)\]?:(?P<port>\d+)\s+(?P<pl>\d+)\s+(?P<ping>\d+)\s+(?P<time_h>\d{1,2}):(?P<time_m>\d{2}):(?P<time_s>\d{2}+)\s+(?P<frags>-?\d+)\s+#(?P<entity_id>\d+)\s+(?P<nickname>.*)$"#
            ).unwrap();
        }
        let mut res = Vec::new();
        if let Some(line) = lines.front() {
            // println!("Trying to parse line {}", line);
            if let Some(caps) = RE.captures(line) {
                let maybe_ip: Result<IpAddr, _> = caps.name("ip").unwrap().as_str().parse();
                let time_h: u32 = caps.name("time_h").unwrap().as_str().parse().unwrap();
                let time_m: u32 = caps.name("time_m").unwrap().as_str().parse().unwrap();
                let time_s: u32 = caps.name("time_s").unwrap().as_str().parse().unwrap();
                if let Ok(ip) = maybe_ip {
                    res.push(RconCmdItem::StatusPlayer {
                        entity_id: caps.name("entity_id").unwrap().as_str().parse().unwrap(),
                        ip,
                        port: caps.name("port").unwrap().as_str().parse().unwrap(),
                        ping: caps.name("ping").unwrap().as_str().parse().unwrap(),
                        pl: caps.name("pl").unwrap().as_str().parse().unwrap(),
                        frags: caps.name("frags").unwrap().as_str().parse().unwrap(),
                        name: String::from(caps.name("nickname").unwrap().as_str()),
                        time: time_h * 60 * 60 + time_m * 60 + time_s,
                    });
                }
            }
        }
        if !res.is_empty() {
            lines.pop_front();
        }
        Some(res)
    }
}

pub fn get_cmd_parser() -> CombinedParser<RconCmdItem> {
    CombinedParser::new(vec![
        Box::new(CvarParser {}),
        Box::new(StatusLineParser {}),
        Box::new(StatusPlayerParser {}),
    ])
}
