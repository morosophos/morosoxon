use std::collections::VecDeque;

pub trait RconDataParser {
    type Item;

    /// Parser should return None if it needs more data,
    /// and a Vec of items otherwise
    fn parse(&mut self, lines: &mut VecDeque<String>) -> Option<Vec<Self::Item>>;
}

pub struct CombinedParser<T> {
    parsers: Vec<Box<dyn RconDataParser<Item = T> + Send + Sync>>,
    active_parser: Option<usize>,
}

impl<T> CombinedParser<T> {
    pub fn new(parsers: Vec<Box<dyn RconDataParser<Item = T> + Send + Sync>>) -> Self {
        Self {
            parsers,
            active_parser: None,
        }
    }
}

impl<T> RconDataParser for CombinedParser<T> {
    type Item = T;
    fn parse(&mut self, lines: &mut VecDeque<String>) -> Option<Vec<Self::Item>> {
        let mut res = Vec::new();
        while !lines.is_empty() {
            let initial_len = lines.len();

            if let Some(active_parser_ix) = self.active_parser {
                // we have a parser which is waiting for more input
                let parser = self.parsers.get_mut(active_parser_ix).unwrap();
                if let Some(v) = parser.parse(lines) {
                    res.extend(v.into_iter());
                    self.active_parser = None
                }
            }

            if self.active_parser.is_none() {
                // we have no active parsers, so let's try if any of them
                // likes the current input
                for (ix, parser) in self.parsers.iter_mut().enumerate() {
                    match parser.parse(lines) {
                        Some(v) => res.extend(v.into_iter()),
                        None => {
                            // need to wait for more input
                            self.active_parser = Some(ix);
                            break;
                        }
                    }
                }
            }

            // no parsers parsed the current input, remove the first line then

            if lines.len() == initial_len {
                lines.pop_front();
            }
        }

        Some(res)
    }
}
