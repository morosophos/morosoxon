pub type EntityId = u32;
pub type CvarName = String;
pub type CvarValue = String;
// pub type EntityField = String;
// pub type EntityFieldValue = String;
pub type MapName = String;
