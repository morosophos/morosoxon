#!/bin/bash

set -ex

cargo build --release --target x86_64-unknown-linux-musl
scp target/x86_64-unknown-linux-musl/release/xinfra xonotic@xon.teichisma.info:xinfra2/
scp -P 54321 target/x86_64-unknown-linux-musl/release/xinfra xonotic@bhop.rip:xinfra2/
