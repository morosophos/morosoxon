// TODO: do we really want and need to mess with parsing versions?
// the only real use case would be to be able to find out the latest version
// but for now lets just go with explicit version specification

#[derive(Debug, Clone, PartialEq)]
pub struct Version {
    pub branch: String,
    pub tag: Option<String>,
    pub commits: Option<u64>,
    pub rev: String,
}
