mod config;
mod errors;
mod git_repo;
mod motd;
mod nginx_root;
mod templates;
mod userdir;
mod utils;
mod versions;

use crate::git_repo::XinfraRepo;
use crate::nginx_root::NginxRoot;
use crate::userdir::Userdir;
use log::info;
use std::fs::File;
use std::io::Read;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt()]
/// Setup xonotic servers declared in xinfra.toml
struct Opt {
    #[structopt(long = "nowww")]
    /// skip www root population
    nowww: bool,
}

fn deploy(opt: &Opt, config: config::Config) -> std::result::Result<(), anyhow::Error> {
    let config_repo = XinfraRepo::new(&config.global.config_repo)?;
    config_repo.pull(false)?;
    for (ix, i) in config.servers.iter().enumerate() {
        info!("Processing server {}", i.name);
        let ud_path = i.get_userdir_path()?;
        let ud = Userdir::new(ud_path)?;
        ud.populate(i, &config)?;
        i.write_server_cfg(&config, &config_repo.get_current_revision_hash()?)?;
        i.write_start_sh(ix)?;
    }

    if !opt.nowww {
        let nginx_root_path = config.global.webserver_root.clone();
        info!(
            "Linking pk3 files into nginx root ({})",
            nginx_root_path.display()
        );
        let nginx_root = NginxRoot::new(nginx_root_path);

        nginx_root.link_data_pk3s(&config)?;
    }
    Ok(())
}

fn main() -> Result<(), anyhow::Error> {
    std::env::set_var("RUST_LOG", "trace");
    pretty_env_logger::init();
    let opt = Opt::from_args();
    let mut config_file = File::open("xinfra.toml")?;
    let mut contents = String::new();
    config_file.read_to_string(&mut contents)?;
    let config: config::Config = toml::from_str(&contents)?;
    info!(
        "Loaded config for {}, {} servers",
        &config.global.public_domain,
        config.servers.len()
    );
    deploy(&opt, config)?;
    Ok(())
}
