pub fn generate_motd(
    basic_motd: &str,
    dp_version: &str,
    game_version: &str,
    cfg: &str,
    cfg_rev: &str,
) -> String {
    let s = format!(
        "{}
Darkplaces version {}
Game version {}
Config {} (https://gitlab.com/morosophos/xonotic-cfg@{})",
        basic_motd, dp_version, game_version, cfg, cfg_rev
    );
    s.replace('\n', r"\n")
}
