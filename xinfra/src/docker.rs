use crate::config::Config;
use dockworker::image::Image;
use dockworker::Docker;
use regex::Regex;
use slog::{debug, info};
use std::path::Path;
use std::process::Command;

// static DOCKER_PREFIX: &str = "xinfra_";

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub struct XonoticVersion {
    release_major: u8,
    release_minor: u8,
    release_subminor: u8,
    revision_num: u16,
    revision_hash: String,
    dirty: bool,
}

impl XonoticVersion {
    pub fn from_string(s: &str) -> Option<Self> {
        lazy_static::lazy_static! {
            static ref RE: Regex = Regex::new(r":?xonotic-v(\d+)\.(\d+)\.(\d+)-(\d+)-g([[:xdigit:]]+)(~?)$").unwrap();

        }
        match RE.captures(s) {
            Some(caps) => Some(Self {
                release_major: caps[1].parse().unwrap(),
                release_minor: caps[2].parse().unwrap(),
                release_subminor: caps[3].parse().unwrap(),
                revision_num: caps[4].parse().unwrap(),
                revision_hash: caps[5].to_owned(),
                dirty: &caps[6] == "~",
            }),
            None => None,
        }
    }

    pub fn to_string(&self) -> String {
        let dirty = if self.dirty { "~" } else { "" };
        format!(
            "xonotic-v{}.{}.{}-{}-g{}{}",
            self.release_major,
            self.release_minor,
            self.release_subminor,
            self.revision_num,
            self.revision_hash,
            dirty
        )
    }
}

pub fn pull_image() -> Result<(), failure::Error> {
    let logger = slog_scope::logger();
    info!(
        logger,
        "Pulling morosophos/xonotic-server from docker registry"
    );
    let script = format!("docker pull -a morosophos/xonotic-server > /dev/null");
    let mut cmd = Command::new("/bin/sh");
    cmd.arg("-c").arg(&script);
    let status = cmd.status()?;
    if status.success() {
        debug!(logger, "Pull successful");
        Ok(())
    } else {
        failure::bail!("Could not pull docker image")
    }
}

pub fn find_latest_image(config: &Config) -> Option<(Image, XonoticVersion)> {
    let docker = Docker::connect_with_defaults().unwrap();
    let images = docker.images(false).unwrap();
    for img in images.iter() {
        let target_tag = format!("{}:latest", config.global.docker_image);
        for tag in img.RepoTags.iter() {
            if tag == &target_tag {
                for i in img.RepoTags.iter() {
                    if let Some(ver) = XonoticVersion::from_string(&i) {
                        return Some((img.clone(), ver));
                    }
                }
            }
        }
    }
    None
}

pub fn copy_csprogs(
    image: &str,
    version: &XonoticVersion,
    dest_dir: &Path,
) -> Result<(), failure::Error> {
    let logger = slog_scope::logger();
    debug!(logger, "Extracting csprogs for {}", version.to_string());
    let script = format!("id=$(docker create {}:{}); docker cp $id:/xonotic/game/data/csprogs-{}.pk3 \"{}\" ; docker rm -v $id > /dev/null",
                        image, version.to_string(), version.to_string(), dest_dir.display());
    let mut cmd = Command::new("/bin/sh");
    cmd.arg("-c").arg(&script);
    let status = cmd.status()?;
    if status.success() {
        info!(
            logger,
            "Successfully extracted csprogs-{}.pk3 to {}",
            version.to_string(),
            dest_dir.display()
        );
        Ok(())
    } else {
        failure::bail!("Could not extract csprogs from docker")
    }
}
