use crate::config::Config;
use crate::config::Server;
use crate::utils::{hardlink_recursive_merge, DataType};
use log::info;
use pk3rs::overrides_config::OverridesConfig;
use pk3rs::PK3;
use std::fs::{create_dir, create_dir_all, read_to_string, remove_dir_all};
use std::io::{Error, ErrorKind};
use std::path::{Path, PathBuf};

pub struct Userdir {
    data: PathBuf,
    data_generated: PathBuf,
    data_static: PathBuf,
}

impl Userdir {
    pub fn new(path: PathBuf) -> Result<Self, anyhow::Error> {
        if path.exists() & (!path.is_dir()) {
            anyhow::bail!(Error::new(
                ErrorKind::Other,
                "Path exists and isn't directory",
            ));
        }
        if !path.exists() {
            create_dir(&path)?;
        }
        info!("Initialized userdir at {}", path.display());
        Ok(Self {
            data: path.join("data"),
            data_generated: path.join("data_generated"),
            data_static: path.join("data_static"),
        })
    }

    fn generate_mapinfo(&self, server: &Server, data_repo: &Path) -> Result<(), anyhow::Error> {
        let destdir = self.data_generated.join("mapinfo.pk3dir/maps");
        info!(
            "Writing mapinfo of server {} to {}",
            server.name,
            destdir.display()
        );
        info!("Clearing old mapinfo");
        if destdir.is_dir() {
            remove_dir_all(&destdir)?;
        }
        let overrides_path = if let Some(ref custom_overrides) = server.overrides {
            data_repo.join(&format!("mapinfo/{}.toml", custom_overrides))
        } else {
            data_repo.join(&format!("mapinfo/{}.toml", server.name))
        };
        let overrides = if overrides_path.is_file() {
            info!("Using overrides {}", overrides_path.display());
            let s = read_to_string(&overrides_path)?;
            Some(OverridesConfig::from_str(&s)?)
        } else {
            None
        };

        if !destdir.exists() {
            create_dir_all(&destdir)?;
        }
        if !destdir.is_dir() {
            anyhow::bail!("{} is not directory", destdir.display());
        }
        for r_entry in walkdir::WalkDir::new(&self.data_static) {
            let entry = r_entry?;
            let path = entry.path();
            if let Some(ext) = path.extension() {
                if ext == "pk3" {
                    let res_pk3 = PK3::from_path(&path);
                    match res_pk3 {
                        Ok(pk3) => {
                            for map in pk3.into_maplist()? {
                                if let Some(mapinfo) =
                                    map.maybe_generate_mapinfo(overrides.as_ref(), true, false)
                                {
                                    let mapinfo_contents = mapinfo.dump();
                                    let destfile = destdir.join(format!("{}.mapinfo", map.name));
                                    std::fs::write(&destfile, mapinfo_contents)?;
                                }
                            }
                        }
                        Err(e) => {
                            println!("Error parsing pk3 {}: {:?}", path.display(), e);
                        }
                    }
                }
            }
        }
        Ok(())
    }

    pub fn populate(&self, server: &Server, config: &Config) -> Result<(), anyhow::Error> {
        if !self.data_static.exists() {
            info!("Creating {}", self.data_static.display());
            create_dir(&self.data_static)?;
        }
        if !self.data_generated.exists() {
            info!("Creating {}", self.data_generated.display());
            create_dir(&self.data_generated)?;
        }
        if !self.data.exists() {
            info!("Creating {}", self.data.display());
            create_dir(&self.data)?;
        }
        let mut sources = vec![
            config.global.config_repo.join("configs"),
            server.get_server_pk3()?,
            server.get_csprogs_pk3()?,
        ];
        for i in server.data_list.iter() {
            let source_path = &config.global.data_repo.join("data").join(i);
            let dt = DataType::from_path(source_path)?;
            let mut s = dt.sources()?;
            sources.append(&mut s);
        }
        info!(
            "Hardlinking data ({} items) into {}",
            sources.len(),
            self.data_static.display()
        );
        hardlink_recursive_merge(&sources, &self.data_static)?;
        info!("Generating mapinfo into {}", self.data_generated.display());
        self.generate_mapinfo(server, &config.global.data_repo)?;
        Ok(())
    }
}
