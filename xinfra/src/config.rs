use crate::motd::generate_motd;
use crate::templates::*;
use handlebars::{no_escape, Handlebars};
use serde::Deserialize;
use serde_json::json;
use std::env::current_dir;
use std::fs::create_dir_all;
use std::os::unix::prelude::PermissionsExt;
use std::path::PathBuf;

#[derive(Deserialize, Debug)]
pub struct Server {
    pub name: String,
    config: String,
    private_config: String,
    pub dp_version: String,
    pub game_version: String,
    hostname: String,
    motd: String,
    pub data_list: Vec<String>,
    public: bool,
    pub overrides: Option<String>,
    dl_url: Option<String>,
    port: Option<usize>,
    noterminal: Option<bool>,
}

impl Server {
    pub fn get_userdir_path(&self) -> Result<PathBuf, std::io::Error> {
        let mut p = current_dir()?;
        p.push(&self.name);
        Ok(p)
    }

    pub fn get_dp_path(&self) -> Result<PathBuf, std::io::Error> {
        let mut p = current_dir()?;
        p.push("dist/dp");
        p.push(&self.dp_version);
        Ok(p)
    }

    pub fn get_game_path(&self) -> Result<PathBuf, std::io::Error> {
        let mut p = current_dir()?;
        p.push("dist/game");
        p.push(&self.game_version);
        Ok(p)
    }

    pub fn get_server_pk3(&self) -> Result<PathBuf, anyhow::Error> {
        let mut game_path = self.get_game_path()?;
        if let Some((_, rev)) = self.game_version.rsplit_once('/') {
            game_path.push(format!("server-{}.pk3", rev));
            Ok(game_path)
        } else {
            anyhow::bail!("Bad game version: {}", &self.game_version)
        }
    }

    pub fn get_csprogs_pk3(&self) -> Result<PathBuf, anyhow::Error> {
        let mut game_path = self.get_game_path()?;
        if let Some((_, rev)) = self.game_version.rsplit_once('/') {
            game_path.push(format!("csprogs-{}.pk3", rev));
            Ok(game_path)
        } else {
            anyhow::bail!("Bad game version: {}", &self.game_version)
        }
    }

    pub fn generate_server_cfg(&self, config: &Config, cfg_rev: &str) -> String {
        let mut hbars = Handlebars::new();
        hbars.register_escape_fn(no_escape);
        let motd = generate_motd(
            &self.motd,
            &self.dp_version,
            &self.game_version,
            &self.config,
            cfg_rev,
        );
        let context = json!({
            "base_config": self.config,
            "private_config": self.private_config,
            "hostname": self.hostname,
            "base_url": self.dl_url.as_ref().unwrap_or(&format!("http://{}:20081/", config.global.public_domain)),
            "motd": motd,
            "public": self.public as u8
        });
        hbars.render_template(SERVER_CFG_TMPL, &context).unwrap()
    }

    pub fn generate_start_sh(&self, ix: usize) -> Result<String, anyhow::Error> {
        let noterminal = if let Some(true) = self.noterminal {
            "-noterminal"
        } else {
            ""
        };
        let mut hbars = Handlebars::new();
        hbars.register_escape_fn(no_escape);
        let context = json!({
            "darkplaces_dir": self.get_dp_path()?,
            "game_dir": self.get_game_path()?,
            "userdir": self.get_userdir_path()?,
            "port": self.port.unwrap_or(26000 + 5 * ix),
            "noterminal": noterminal
        });
        Ok(hbars.render_template(START_SH_TMPL, &context).unwrap())
    }

    pub fn write_start_sh(&self, ix: usize) -> Result<(), anyhow::Error> {
        let mut p = self.get_userdir_path()?;
        p.push("start.sh");
        std::fs::write(&p, &self.generate_start_sh(ix)?)?;
        let metadata = std::fs::metadata(&p)?;
        let mut perms = metadata.permissions();
        perms.set_mode(0o755);
        std::fs::set_permissions(&p, perms)?;
        Ok(())
    }

    pub fn write_server_cfg(&self, config: &Config, cfg_rev: &str) -> Result<(), std::io::Error> {
        let mut p = self.get_userdir_path()?;
        let server_cfg = self.generate_server_cfg(config, cfg_rev);
        p.push("data");
        if !p.exists() {
            create_dir_all(&p)?;
        }
        p.push("server.cfg");
        std::fs::write(&p, &server_cfg)?;
        Ok(())
    }
}

#[derive(Deserialize, Debug)]
pub struct GlobalSettings {
    pub public_domain: String,
    pub webserver_root: PathBuf,
    pub config_repo: PathBuf,
    pub data_repo: PathBuf,
}

#[derive(Deserialize, Debug)]
pub struct Config {
    pub global: GlobalSettings,
    pub servers: Vec<Server>,
}
