use std::io;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum XinfraError {
    #[error("IO Error: {0}")]
    IoError(#[from] io::Error)
}
