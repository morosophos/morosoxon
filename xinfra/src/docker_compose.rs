use std::collections::HashMap;
use std::path::Path;

use crate::config::Config;

// the code below is purposedly simplistic cause I don't plan to use docker-compose in future

#[derive(serde_derive::Deserialize, serde_derive::Serialize, Debug)]
struct Service {
    image: String,
    container_name: String,
    restart: String,
    volumes: Vec<String>,
    ports: Vec<String>,
    cap_add: Vec<String>,
    network_mode: String,
    environment: Vec<String>,
}

impl Service {
    fn new(
        name: &str,
        docker_image: &str,
        version: Option<&str>,
        userdir_path: &Path,
        port: u16,
        rcon_password: &str,
        rcon_restricted_password: &str,
    ) -> Self {
        let image = match version {
            Some(v) => format!("{}:{}", docker_image, v),
            None => format!("{}:latest", docker_image),
        };
        Self {
            image,
            container_name: name.to_owned(),
            restart: "always".to_owned(),
            volumes: vec![format!("{}:/xonotic/userdir", userdir_path.display())],
            ports: vec![],
            cap_add: vec!["SYS_NICE".to_owned()],
            network_mode: "host".to_owned(),
            environment: vec![
                format!("PORT={}", port),
                format!("RCON_PASSWORD={}", rcon_password),
                format!("RCON_RESTRICTED_PASSWORD={}", rcon_restricted_password)
            ],
        }
    }
}

#[derive(serde_derive::Deserialize, serde_derive::Serialize, Debug)]
pub struct DockerCompose {
    version: String,
    services: HashMap<String, Service>,
}

impl DockerCompose {
    pub fn new(config: &Config) -> Self {
        let mut port: u16 = 26000;
        let port_step = 5;
        let mut services = HashMap::new();
        services.insert(
            "nginx".to_owned(),
            Service {
                image: "nginx:1.15.8-alpine".to_owned(),
                container_name: "xinfra-nginx".to_owned(),
                restart: "always".to_owned(),
                volumes: vec![format!(
                    "{}:/usr/share/nginx/html:ro",
                    &config.global.webserver_root.display()
                )],
                ports: vec!["20080:80".to_owned()],
                cap_add: vec![],
                network_mode: "bridge".to_owned(),
                environment: vec![],
            },
        );
        for i in config.servers.iter() {
            services.insert(
                i.name.clone(),
                Service::new(
                    &i.name,
                    &config.global.docker_image,
                    i.version.as_deref(),
                    &i.get_userdir_path().unwrap(),
                    port,
                    &i.rcon_password,
                    i.rcon_restricted_password.as_deref().unwrap_or("")
                ),
            );
            port += port_step;
        }
        Self {
            version: "3.2".to_owned(),
            services,
        }
    }
}
