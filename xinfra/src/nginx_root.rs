use crate::config::Config;
use std::fs::hard_link;
use std::path::PathBuf;
use walkdir::WalkDir;

pub struct NginxRoot {
    path: PathBuf,
}

impl NginxRoot {
    pub fn new(path: PathBuf) -> Self {
        Self { path }
    }

    pub fn link_data_pk3s(&self, config: &Config) -> Result<(), anyhow::Error> {
        for i in WalkDir::new(&config.global.data_repo.join("data")) {
            let entry = i?;
            if entry.file_type().is_file() {
                if let Some(ext) = entry.path().extension() {
                    if ext == "pk3" {
                        let dest = self.path.join(entry.file_name());
                        if dest.exists() {
                            std::fs::remove_file(&dest)?;
                        }
                        hard_link(entry.path().canonicalize()?, dest)?;
                    }
                }
            }
        }
        for i in config.servers.iter() {
            let csprogs_path = i.get_csprogs_pk3()?;
            let dest = self.path.join(csprogs_path.file_name().unwrap());
            if dest.exists() {
                std::fs::remove_file(&dest)?;
            }
            hard_link(csprogs_path, dest)?;
        }
        Ok(())
    }
}
