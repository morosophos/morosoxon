pub static SERVER_CFG_TMPL: &str = r#"exec "configs/{{ base_config }}"
sv_public "{{ public }}"
sv_curl_defaulturl "{{ base_url }}"
hostname "{{ hostname }}"
sv_motd "{{ motd }}"
{{ private_config }}
"#;

pub static START_SH_TMPL: &str = r#"#!/bin/bash
#!/bin/sh

set -ex
ulimit -n 20480

cd {{darkplaces_dir}}
DARKPLACES=./darkplaces-dedicated

ARGS="-xonotic -userdir {{userdir}} -game data_static -game data_generated -game data {{noterminal}}"

export LD_LIBRARY_PATH="{{darkplaces_dir}}/libs"

exec "$DARKPLACES" $ARGS +port "{{port}}"
"#;
