use std::collections::HashMap;
use std::fs::{create_dir_all, hard_link, metadata, remove_dir_all, remove_file};
use std::os::unix::fs::MetadataExt;
use std::path::{Path, PathBuf};
use walkdir::WalkDir;

// fn is_hidden(entry: &DirEntry) -> bool {
//     entry
//         .file_name()
//         .to_str()
//         .map(|s| s.starts_with("."))
//         .unwrap_or(false)
// }

fn hardlink_resolve_conflict(spath: &Path, dpath: &Path) -> Result<(), anyhow::Error> {
    if dpath.exists() {
        let smd = metadata(spath)?;
        let dmd = metadata(dpath)?;
        if smd.ino() == dmd.ino() {
            // hard links to the same file
            return Ok(());
        } else {
            log::info!("Removing conflicting path {}", dpath.display());
            if smd.file_type().is_dir() {
                remove_dir_all(dpath)?;
            } else {
                remove_file(dpath)?;
            }
        }
    }
    log::debug!(
        "Hardlinking {} to {}",
        spath.display(),
        dpath.display()
    );

    hard_link(spath, dpath)?;
    Ok(())
}

pub fn hardlink_recursive_merge<P: AsRef<Path>>(
    sources: &[P],
    dest_dir: &Path,
) -> Result<(), anyhow::Error> {
    let mut dup_checker = HashMap::new();
    // Hardlink everything from sources to dest_dir
    // Walk through dest_dir and remove everything which isn't present at sources
    // Watch for conflicts
    for i in sources {
        let source = i.as_ref();
        let swalker = WalkDir::new(source).into_iter();
        let source_file_name = source.file_name().unwrap();
        let target = dest_dir.join(source_file_name);
        if source.is_file() {
            dup_checker.insert(
                Path::new(source_file_name).to_path_buf(),
                source.to_path_buf(),
            );
            hardlink_resolve_conflict(source, &target)?;
        } else {
            for e in swalker {
                let entry = e?;
                // unwrapping cause the source_path prefix should be always there, otherwise it's a bug in walkdir or this code
                let rel_path = Path::new(source_file_name)
                    .join(entry.path().strip_prefix(source).unwrap())
                    .to_path_buf();
                dup_checker.insert(rel_path.clone(), entry.path().to_path_buf());
                if entry.file_type().is_file() {
                    if let Some(dir) = rel_path.ancestors().nth(1) {
                        create_dir_all(&dest_dir.join(dir))?;
                    }
                    let dpath = dest_dir.join(rel_path);
                    hardlink_resolve_conflict(entry.path(), &dpath)?;
                }
            }
        }
    }
    let dwalker = WalkDir::new(&dest_dir).min_depth(1);
    for e in dwalker {
        let entry = e?;
        let rel_path = entry.path().strip_prefix(dest_dir).unwrap();
        let k = rel_path.to_path_buf();
        if !dup_checker.contains_key(&k) {
            if entry.file_type().is_dir() {
                remove_dir_all(entry.path())?;
            } else {
                remove_file(entry.path())?;
            }
        }
    }
    Ok(())
}

pub enum DataType<'a> {
    Collection(&'a Path),
    Pk3(&'a Path),
    Pk3Dir(&'a Path),
}

impl<'a> DataType<'a> {
    pub fn from_path(p: &'a Path) -> Result<Self, anyhow::Error> {
        if !p.exists() {
            anyhow::bail!(std::io::Error::new(
                std::io::ErrorKind::NotFound,
                format!("{:?} does not exist", p),
            ));
        }
        if let Some(ext) = p.extension() {
            if ext == "pk3" {
                return Ok(DataType::Pk3(p));
            } else if ext == "pk3dir" {
                return Ok(DataType::Pk3Dir(p));
            } else if ext == "collection" {
                return Ok(DataType::Collection(p));
            }
        }
        anyhow::bail!(std::io::Error::new(
            std::io::ErrorKind::NotFound,
            format!("Wrong type: {:?}", p),
        ))
    }

    pub fn sources(&self) -> Result<Vec<PathBuf>, anyhow::Error> {
        let mut res = Vec::new();
        match self {
            DataType::Pk3(p) => res.push(p.to_path_buf()),
            DataType::Pk3Dir(p) => res.push(p.to_path_buf()),
            DataType::Collection(p) => {
                let w = WalkDir::new(p).min_depth(1).max_depth(1).into_iter();
                for i in w {
                    let e = i?;
                    let d = DataType::from_path(e.path())?;
                    res.append(&mut d.sources()?);
                }
            }
        }
        Ok(res)
    }
}
