use anyhow::{bail, format_err, Error};
use git2::{ObjectType, Repository};
// use git2::BranchType;
use std::env::{current_dir, set_current_dir};
use std::path::Path;
use std::process::Command;

pub struct XinfraRepo {
    repo: Repository,
}

#[derive(PartialEq, Debug)]
pub enum PullResult {
    UNCHANGED,
    UPDATED(String),
}

impl XinfraRepo {
    pub fn new(p: &Path) -> Result<Self, Error> {
        Ok(Self {
            repo: Repository::discover(p)?,
        })
    }

    pub fn get_current_revision_hash(&self) -> Result<String, Error> {
        let obj = self.repo.head()?.resolve()?.peel(ObjectType::Commit)?;
        let commit = obj.into_commit().map_err(|e| format_err!("{:?}", e))?;
        let mut hash = format!("{}", commit.id());
        hash.truncate(10);
        Ok(hash)
    }

    fn pull_inner(&self, lfs_checkout: bool) -> Result<PullResult, Error> {
        let pull_script = "git pull --ff-only -q";
        let lfs_checkout_script = "git lfs checkout";
        let workdir = if let Some(wd) = self.repo.workdir() {
            wd
        } else {
            bail!("Repository has no workdir")
        };
        set_current_dir(workdir)?;
        let old_rev = self.get_current_revision_hash()?;
        let res = {
            let mut cmd = Command::new("/bin/sh");
            cmd.arg("-c").arg(pull_script);
            let status = cmd.status()?;
            match status.code() {
                Some(0) => {
                    let new_rev = self.get_current_revision_hash()?;
                    if old_rev == new_rev {
                        Ok(PullResult::UNCHANGED)
                    } else {
                        Ok(PullResult::UPDATED(new_rev))
                    }
                }
                _ => bail!("Failed to fastforward repo, please merge manually"),
            }
        };
        if lfs_checkout {
            let mut cmd = Command::new("/bin/sh");
            cmd.arg("-c").arg(lfs_checkout_script);
            let status = cmd.status()?;
            if status.success() {
                res
            } else {
                bail!("Error during lfs checkout");
            }
        } else {
            res
        }
    }

    pub fn pull(&self, lfs_checkout: bool) -> Result<PullResult, Error> {
        let cwd = current_dir()?;
        let res = self.pull_inner(lfs_checkout);
        set_current_dir(&cwd)?;
        res

        // In case you ever need to authenticate fetch from remote:
        // Note, that this will require linking to huge amount of dynamic libs
        // let mut cbs = RemoteCallbacks::new();
        // cbs.credentials(|url, username, ct| {
        //     Cred::ssh_key(username.unwrap(),
        //     Some(Path::new("/home/user/.ssh/id_rsa.pub")),
        //     Path::new("/home/user/.ssh/id_rsa"), None)
        // });
        // let mut fetch_options = FetchOptions::new();
        // fetch_options.remote_callbacks(cbs);
        // self.repo.find_remote("origin")?.fetch(&["master"], Some(&mut fetch_options), None)?;
        // self.repo
        //     .find_remote("origin")?
        //     .fetch(&["master"], None, None)?;
        // let master = self.repo.find_branch("master", BranchType::Local)?;
        // let upstream = master.upstream()?;
        // let target_master = master
        //     .get()
        //     .target()
        //     .ok_or_else(|| format_err!("Couldn't get local master oid"))?;
        // let target_upstream = upstream
        //     .get()
        //     .target()
        //     .ok_or_else(|| format_err!("Couldn't get upstream master oid"))?;
        // if target_master == target_upstream {
        //     Ok(PullResult::UNCHANGED)
        // } else {
        //     let mut head = self.repo.head()?;
        //     let mut checkout_opts = git2::build::CheckoutBuilder::new();
        //     checkout_opts.force();
        //     let target = upstream.get().peel(ObjectType::Commit)?;
        //     self.repo.checkout_tree(&target, Some(&mut checkout_opts))?;
        //     head.set_target(target_upstream, "Fast forward")?;
        //     Ok(PullResult::UPDATED(format!("{}", target_upstream)))
        // }
    }
}
