use crate::crc::crc_block;
use dpcolors::DPString;
use lazy_static::lazy_static;
use num_traits::identities::Zero;
use percent_encoding::{percent_decode, percent_encode, NON_ALPHANUMERIC};
use regex::Regex;
use rust_decimal::Decimal;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::io::Write;
use std::path::Path;
use std::str::FromStr;
use thiserror::Error;

const DEFAULT_BUCKET_SIZE: usize = 8192;

#[derive(Debug, Clone)]
pub struct TimeRecord {
    pub pos: u16,
    pub time: Decimal,
    pub crypto_idfp: CryptoIDFP,
}

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
pub struct Map(pub String);

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
pub struct CryptoIDFP(pub String);

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
struct MapPos {
    map: Map,
    pos: u16,
}

impl MapPos {
    fn new(s: &str, p: u16) -> Self {
        Self {
            map: Map(s.to_owned()),
            pos: p,
        }
    }
}

#[derive(Debug, Clone)]
pub struct SpeedRecord {
    pub crypto_idfp: CryptoIDFP,
    pub speed: Decimal,
}

#[derive(Default)]
pub struct ServerDB {
    pub time_records: HashMap<Map, Vec<Option<TimeRecord>>>,
    pub speed_records: HashMap<Map, SpeedRecord>,
    pub players: HashMap<CryptoIDFP, (String, DPString)>,
    crypto_idfp_n: HashMap<MapPos, CryptoIDFP>,
    time_n: HashMap<MapPos, Decimal>,
    speed_crypto_idfp: HashMap<Map, CryptoIDFP>,
    speed_speed: HashMap<Map, Decimal>,
}

#[derive(Debug, Error)]
pub enum ServerDBError {
    ReadError,
    ParseError,
    InvalidHeader,
    Empty,
    Incomplete,
    Bloated, // more lines than heading specifies
}

impl std::fmt::Display for ServerDBError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl ServerDB {
    fn get_bufreader(path: &Path) -> Result<io::BufReader<File>, io::Error> {
        let f = File::open(path)?;
        Ok(io::BufReader::new(f))
    }

    #[deprecated]
    pub fn empty() -> Self {
        Default::default()
    }

    pub fn new<T: AsRef<Path>>(path: T) -> Result<Self, ServerDBError> {
        let mut br = Self::get_bufreader(path.as_ref()).map_err(|_| ServerDBError::ReadError)?;
        let mut db: Self = Default::default();
        let mut buf = String::new();
        br.read_line(&mut buf)
            .map_err(|_| ServerDBError::ReadError)?;
        let _buckets: u16 = buf
            .trim()
            .parse()
            .map_err(|_| ServerDBError::InvalidHeader)?;
        let mut _total_lines = 0;
        loop {
            let mut buf = String::new();
            match br.read_line(&mut buf) {
                Ok(n) => {
                    if n > 0 {
                        if !buf.ends_with('\n') {
                            return Err(ServerDBError::Incomplete);
                        }
                        db.parse_line(buf.trim_end_matches('\n'))?;
                        println!("Parsed {} lines", _total_lines);
                    } else {
                        break;
                    }
                }
                Err(_) => return Err(ServerDBError::ReadError),
            }
            _total_lines += 1;
        }
        db.crypto_idfp_n.clear();
        db.crypto_idfp_n.shrink_to_fit();
        db.time_n.clear();
        db.time_n.shrink_to_fit();
        Ok(db)
        // match total_lines.cmp(&buckets) {
        //     Ordering::Greater => Err(ServerDBError::Bloated),
        //     Ordering::Less => Err(ServerDBError::Incomplete),
        //     Ordering::Equal => {
        //         db.crypto_idfp_n.clear();
        //         db.crypto_idfp_n.shrink_to_fit();
        //         db.time_n.clear();
        //         db.time_n.shrink_to_fit();
        //         Ok(db)
        //     }
        // }
    }

    pub fn merge_with_other_dbs(&mut self, other_dbs: &[ServerDB]) {
        for db in other_dbs.iter() {
            for (map, records) in db.time_records.iter() {
                for r in records.iter().flatten() {
                    if let Some((raw_nickname, _)) = db.players.get(&r.crypto_idfp) {
                        self.insert_record(raw_nickname, map.clone(), r.clone());
                    }
                }
            }
            for (map, speed_record) in db.speed_records.iter() {
                if let Some((raw_nickname, _)) = db.players.get(&speed_record.crypto_idfp) {
                    self.insert_speed_record(raw_nickname, map.clone(), speed_record.clone());
                }
            }
        }
        self.check_consistency(true);
    }

    pub fn insert_record(&mut self, raw_nickname: &str, map: Map, record: TimeRecord) {
        self.players.insert(
            record.crypto_idfp.clone(),
            (String::from(raw_nickname), DPString::parse(raw_nickname)),
        );
        let existing_time_records = self.time_records.entry(map).or_insert_with(Vec::new);
        existing_time_records
            .sort_by(|a, b| a.as_ref().unwrap().time.cmp(&b.as_ref().unwrap().time));
        // 2. does he already has a record on this map?
        // 3. is his db record the same as imported record?
        for i in existing_time_records.iter() {
            let i = i.as_ref().unwrap(); // let's assume the DB is consistent
            if (i.crypto_idfp == record.crypto_idfp) && (i.time <= record.time) {
                // existing record is better or equal, no need to do anything
                return;
            }
        }

        let mut pos = 0;
        for (ix, i) in existing_time_records.iter().enumerate() {
            let i = i.as_ref().unwrap(); // let's assume the DB is consistent
            if i.time < record.time {
                pos = ix + 1;
            }
        }

        existing_time_records.insert(
            pos,
            Some(TimeRecord {
                pos: (pos + 1) as u16,
                time: record.time,
                crypto_idfp: record.crypto_idfp,
            }),
        );
        pos += 1;
        while pos < existing_time_records.len() {
            existing_time_records[pos].as_mut().unwrap().pos += 1;
            pos += 1;
        }
    }

    pub fn insert_speed_record(&mut self, raw_nickname: &str, map: Map, record: SpeedRecord) {
        let maybe_existing_speed_record = self.speed_records.get(&map);
        let need_update = match maybe_existing_speed_record {
            Some(sr) => sr.speed < record.speed,
            None => true,
        };
        if need_update {
            if !self.players.contains_key(&record.crypto_idfp) {
                self.players.insert(
                    record.crypto_idfp.clone(),
                    (String::from(raw_nickname), DPString::parse(raw_nickname)),
                );
            }
            self.speed_records.insert(map, record);
        }
    }

    fn to_key_value(&self) -> HashMap<String, String> {
        let mut hm = HashMap::new();
        for (map, records) in self.time_records.iter() {
            for r in records.iter().flatten() {
                let t = (r.time * Decimal::new(100, 0)).floor();
                hm.insert(
                    format!("{}/cts100record/time{}", map.0, r.pos),
                    t.to_string(),
                );
                hm.insert(
                    format!("{}/cts100record/crypto_idfp{}", map.0, r.pos),
                    r.crypto_idfp.0.clone(),
                );
            }
        }
        for (map, record) in self.speed_records.iter() {
            hm.insert(
                format!("{}/cts100record/speed/speed", map.0),
                record.speed.to_string(),
            );
            hm.insert(
                format!("{}/cts100record/speed/crypto_idfp", map.0),
                record.crypto_idfp.0.clone(),
            );
        }
        for (crypto_idfp, player) in self.players.iter() {
            hm.insert(format!("/uid2name/{}", crypto_idfp.0), player.0.clone());
        }
        // println!("{:?}", hm);
        hm
    }

    pub fn save<T: AsRef<Path>>(&self, path: T) -> Result<(), std::io::Error> {
        let mut lines = Vec::with_capacity(8193);
        for _i in 0..DEFAULT_BUCKET_SIZE {
            lines.push(Vec::new());
        }
        let mut f = File::create(path)?;
        let hm = self.to_key_value();
        for (key, value) in hm.iter() {
            let line_num = (crc_block(key.as_bytes()) % 8192) as usize;
            let formatted = format!(
                "\\{}\\{}",
                key,
                percent_encode(value.as_bytes(), NON_ALPHANUMERIC)
            );
            lines[line_num].push(formatted);
        }
        f.write_all(format!("{}\n", DEFAULT_BUCKET_SIZE).as_bytes())?;
        for line in lines.iter_mut() {
            line.sort_by_key(String::len);
            for part in line.iter() {
                f.write_all(part.as_bytes())?;
            }
            f.write_all(b"\n")?;
        }
        Ok(())
    }

    fn insert_time_record(&mut self, m: String, crypto_idfp: CryptoIDFP, pos: u16, time: Decimal) {
        let time_record = TimeRecord {
            pos,
            crypto_idfp,
            time,
        };
        let pos = pos as usize;
        let map = Map(m);
        let v = self.time_records.entry(map).or_insert_with(Vec::new);
        if v.len() < pos - 1 {
            for _ in v.len()..pos - 1 {
                v.push(None);
            }
        }
        if v.len() < pos {
            v.push(Some(time_record));
        } else {
            v[pos - 1] = Some(time_record);
        }
    }

    pub fn deduplicate(&mut self, dup_id_map: &HashMap<CryptoIDFP, CryptoIDFP>) {
        for (_map, records) in self.time_records.iter_mut() {
            for maybe_record in records.iter_mut() {
                if let Some(rec) = maybe_record.as_mut() {
                    if let Some(new_id) = dup_id_map.get(&rec.crypto_idfp) {
                        rec.crypto_idfp = new_id.clone();
                    }
                }
            }
        }
        for (_map, record) in self.speed_records.iter_mut() {
            if let Some(new_id) = dup_id_map.get(&record.crypto_idfp) {
                record.crypto_idfp = new_id.clone();
            }
        }
        self.check_consistency(true);
    }

    pub fn check_consistency(&mut self, fix: bool) {
        let mut fixed_records = HashMap::new();
        for (map, records) in self.time_records.iter() {
            let mut problem_detected = false;
            let mut cur_time = &Decimal::zero();
            let mut players = HashSet::new();
            for (ix, maybe_record) in records.iter().enumerate() {
                match maybe_record {
                    Some(record) => {
                        if players.contains(&record.crypto_idfp) {
                            problem_detected = true;
                        } else {
                            players.insert(record.crypto_idfp.clone());
                        }
                        if record.time >= *cur_time {
                            cur_time = &record.time;
                        } else {
                            problem_detected = true;
                        }
                    }
                    None => {
                        problem_detected = true;
                        println!(
                            "Missing record on map {:?} pos {}, total records {}",
                            map,
                            ix,
                            records.len()
                        )
                    }
                }
            }
            if problem_detected {
                if fix {
                    let mut new_records = Vec::new();
                    for r in records.iter().flatten() {
                        if r.time > Decimal::zero() {
                            new_records.push(r.clone());
                        }
                    }
                    new_records.sort_by(|a, b| a.time.cmp(&b.time));
                    let mut players = HashSet::new();
                    new_records.retain(|r| {
                        if players.contains(&r.crypto_idfp) {
                            false
                        } else {
                            players.insert(r.crypto_idfp.clone());
                            true
                        }
                    });

                    let mut pos = 1;
                    for i in new_records.iter_mut() {
                        i.pos = pos;
                        pos += 1;
                    }

                    fixed_records.insert(
                        map.clone(),
                        new_records.iter().map(|x| Some(x.clone())).collect(),
                    );
                }
            } else {
                fixed_records.insert(map.clone(), records.clone());
            }
        }

        if fix {
            self.time_records = fixed_records;
        }
    }

    pub fn get_active_crypto_idfps(&self) -> HashSet<CryptoIDFP> {
        let mut hm = HashSet::new();
        for (_map, records) in self.time_records.iter() {
            for record in records.iter().flatten() {
                hm.insert(record.crypto_idfp.clone());
            }
        }
        for (_map, record) in self.speed_records.iter() {
            hm.insert(record.crypto_idfp.clone());
        }
        hm
    }

    pub fn print_all_users(&self) {
        let active_ids = self.get_active_crypto_idfps();
        for (id, player) in self.players.iter() {
            if active_ids.contains(id) {
                println!("{} {}", id.0, player.1.to_none());
            }
        }
    }

    pub fn cleanup_uid2name(&mut self) {
        let active_ids = self.get_active_crypto_idfps();
        self.players.retain(|id, _| active_ids.contains(id));
    }

    pub fn add_uid2name(&mut self, crypto_idfp: CryptoIDFP, nickname: String) {
        let dpstring = DPString::parse(&nickname);
        self.players.insert(crypto_idfp, (nickname, dpstring));
    }

    pub fn health_check(&mut self, fix: bool) {
        // check consistency of time records (ordering, no gaps)
        self.check_consistency(fix);
        // check that all crypto_idfps have uid2name record
        // remove stray uid2names
        // detect possible dups
    }

    fn parse_line(&mut self, line: &str) -> Result<(), ServerDBError> {
        lazy_static! {
            static ref KEYPAIR_RE: Regex = Regex::new(r#"\\([^\\"]+)\\([^\\"]+)"#).unwrap();
            static ref CRYPTO_IDFP_N_RE: Regex =
                Regex::new(r"(.+)/cts100record/crypto_idfp(\d+)").unwrap();
            static ref TIME_N_RE: Regex = Regex::new(r"(.+)/cts100record/time(\d+)").unwrap();
            static ref UID2NAME_RE: Regex = Regex::new(r"/uid2name/(.+)").unwrap();
            static ref SPEEDRECORD_SPEED_RE: Regex =
                Regex::new(r"(.+)/cts100record/speed/speed").unwrap();
            static ref SPEEDRECORD_PLAYER_RE: Regex =
                Regex::new(r"(.+)/cts100record/speed/crypto_idfp").unwrap();
        }
        for c in KEYPAIR_RE.captures_iter(line) {
            let key = &c[1];
            let value = percent_decode(c[2].as_bytes()).decode_utf8_lossy();
            println!("Key {} value {}", key, value);
            let mut new_time_record = None;
            let mut new_speed_record = None;
            if let Some(caps) = UID2NAME_RE.captures(key) {
                self.players.insert(
                    CryptoIDFP(caps[1].to_string()),
                    (value.to_string(), DPString::parse(&value)),
                );
            } else if let Some(caps) = TIME_N_RE.captures(key) {
                let m = caps[1].to_string();
                let pos = caps[2].to_string().parse().unwrap();
                let mut time = Decimal::from_str(&value).unwrap();
                time.set_scale(2).unwrap();

                self.time_n.insert(MapPos::new(&m, pos), time);
                if let Some(crypto_idfp) = self.crypto_idfp_n.get(&MapPos::new(&m, pos)) {
                    new_time_record = Some((m, crypto_idfp.clone(), pos, time));
                }
            } else if let Some(caps) = CRYPTO_IDFP_N_RE.captures(key) {
                let m = caps[1].to_string();
                let pos = caps[2].to_string().parse().unwrap();
                let crypto_idfp = CryptoIDFP(value.into_owned());
                self.crypto_idfp_n
                    .insert(MapPos::new(&m, pos), crypto_idfp.clone());
                if let Some(time) = self.time_n.get(&MapPos::new(&m, pos)) {
                    new_time_record = Some((m, crypto_idfp, pos, *time));
                }
            } else if let Some(caps) = SPEEDRECORD_SPEED_RE.captures(key) {
                let m = Map(caps[1].to_string());
                let speed = Decimal::from_str(&value).unwrap();
                self.speed_speed.insert(m.clone(), speed);
                if let Some(crypto_idfp) = self.speed_crypto_idfp.get(&m) {
                    new_speed_record = Some((m, crypto_idfp.clone(), speed));
                }
            } else if let Some(caps) = SPEEDRECORD_PLAYER_RE.captures(key) {
                let m = Map(caps[1].to_string());
                let crypto_idfp = CryptoIDFP(value.to_string());
                self.speed_crypto_idfp
                    .insert(m.clone(), crypto_idfp.clone());
                if let Some(speed) = self.speed_speed.get(&m) {
                    new_speed_record = Some((m, crypto_idfp, *speed));
                }
            }
            if let Some(nr) = new_time_record {
                self.insert_time_record(nr.0, nr.1, nr.2, nr.3);
            }
            if let Some(sr) = new_speed_record {
                self.speed_records.insert(
                    sr.0,
                    SpeedRecord {
                        crypto_idfp: sr.1,
                        speed: sr.2,
                    },
                );
            }
        }
        Ok(())
    }
}
