use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
// use std::path::Path;
use xonotic_db::server_db;

fn _read_dup_id_map(
    path: &str,
) -> std::io::Result<HashMap<server_db::CryptoIDFP, server_db::CryptoIDFP>> {
    let f = File::open(path)?;
    let reader = BufReader::new(f);
    let mut hm = HashMap::new();
    for r_line in reader.lines() {
        let line = r_line?;
        let mut iter = line.split_whitespace();
        let old_id = iter.next().unwrap();
        let new_id = iter.next().unwrap();
        hm.insert(
            server_db::CryptoIDFP(old_id.to_owned()),
            server_db::CryptoIDFP(new_id.to_owned()),
        );
    }
    Ok(hm)
}

fn main() {
    let mut db = server_db::ServerDB::new("server.db").unwrap();
    // db.merge(&read_dup_id_map("dup-db.txt").unwrap());

    db.add_uid2name(
        server_db::CryptoIDFP(String::from("5CQzz8kQBNOM2B3IcoVCwn9K77jWCZRkP0U896vi2E0=")),
        String::from("johnnyB"),
    );
    db.save("server.db.fixed").unwrap();
    // db.print_all_users();
    // for (map, records) in db.time_records.iter() {
    //     let mut has_mirio = false;
    //     for maybe_record in records.iter() {
    //         if let Some(record) = maybe_record {
    //             if record.crypto_idfp.0 == "g5go3igKoJGNiSdlJYHXLXYDI9px0jSs8B/K7gfw5hg=" {
    //                 has_mirio = true;
    //             }
    //         }
    //     }
    //     if !has_mirio {
    //         println!("{:?}", map);
    //     }
    // }
}
