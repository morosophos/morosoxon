use dpcolors::DPString;
use serde::Deserialize;
use std::path::PathBuf;
use std::str::FromStr;
use structopt::StructOpt;
use xonotic_db::server_db;

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(name = "CSV_PATH", parse(from_os_str))]
    csv_path: PathBuf,
    #[structopt(name = "SOURCE_DB", parse(from_os_str))]
    source_db_path: PathBuf,
    #[structopt(name = "OUTPUT_DB", parse(from_os_str))]
    output_db_path: PathBuf,
}

#[derive(Debug, Deserialize)]
struct OldRecord {
    time: String,
    map: String,
    crypto_idfp: String,
    nickname: String,
}

fn insert_record(db: &mut server_db::ServerDB, record: OldRecord) {
    let crypto_idfp = server_db::CryptoIDFP(record.crypto_idfp);
    let mut record_time = rust_decimal::Decimal::from_str(&record.time)
        .expect(&format!("Invalid record time {:?}", record.time));
    record_time.set_scale(2).unwrap();
    // 1. do we have this guy in uid2name?
    if !db.players.contains_key(&crypto_idfp) {
        db.players.insert(
            crypto_idfp.clone(),
            (record.nickname.clone(), DPString::parse(&record.nickname)),
        );
    }
    let map = server_db::Map(record.map);
    let existing_time_records = db.time_records.entry(map).or_insert_with(Vec::new);
    existing_time_records.sort_by(|a, b| a.as_ref().unwrap().time.cmp(&b.as_ref().unwrap().time));
    // 2. does he already has a record on this map?
    // 3. is his db record the same as imported record?
    for i in existing_time_records.iter() {
        let i = i.as_ref().unwrap(); // let's assume the DB is consistent
        if (i.crypto_idfp == crypto_idfp) && (i.time <= record_time) {
            // existing record is better or equal, no need to do anything
            return;
        }
    }

    let mut pos = 0;
    for (ix, i) in existing_time_records.iter().enumerate() {
        let i = i.as_ref().unwrap(); // let's assume the DB is consistent
        if i.time < record_time {
            pos = ix + 1;
        }
    }

    existing_time_records.insert(
        pos,
        Some(server_db::TimeRecord {
            pos: (pos + 1) as u16,
            time: record_time,
            crypto_idfp,
        }),
    );
    pos += 1;
    while pos < existing_time_records.len() {
        existing_time_records[pos].as_mut().unwrap().pos += 1;
        pos += 1;
    }
}

fn main() -> Result<(), anyhow::Error> {
    let opt = Opt::from_args();
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .delimiter(b';')
        .from_path(&opt.csv_path)?;
    let old_records: Vec<OldRecord> = rdr
        .deserialize()
        .map(|x| x.expect("Could not parse record"))
        .collect();
    let mut db = server_db::ServerDB::new(&opt.source_db_path)?;
    for i in old_records {
        insert_record(&mut db, i);
    }
    db.check_consistency(true);
    db.save(&opt.output_db_path)?;
    Ok(())
}
