use std::path::PathBuf;
use structopt::StructOpt;
use xonotic_db::server_db::ServerDB;

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(name = "TARGET_DB", parse(from_os_str))]
    target_db: PathBuf,
    #[structopt(name = "SOURCE_DBS", parse(from_os_str))]
    source_dbs: Vec<PathBuf>,
}

fn main() -> Result<(), anyhow::Error> {
    let opt = Opt::from_args();
    let mut target_db = ServerDB::new(&opt.target_db)?;
    let mut other_dbs = Vec::new();
    for i in opt.source_dbs {
        other_dbs.push(ServerDB::new(&i)?);
    }
    target_db.merge_with_other_dbs(&other_dbs);
    target_db.save(&opt.target_db)?;
    Ok(())
}
