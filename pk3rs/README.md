# pk3rs - reliable and fast mapinfo generation for Xonotic

## Why

Xonotic can generate mapinfo files itself, and its heuristics are reasonable. But the QC implementation crashes on large maps (you can easily crash your server by adding a new map) and it's rather slow overall. Also I need some tools for managing my defrag mapinfo, hence I'm writing this utility

## Implementation

This tool is implemented in Rust and uses proper parsing techologies  - nom for text formats (entities and dist mapinfo). It also reads entity lump offset and size from the BSP header instead of iterating through the file trying to finding "{", like the QC implementation does. The heuristics were ported from mapinfo.qc/mapinfo.qh and left almost intact.

Performance is okish - the tool needs ~10 seconds to process the batch of maps from the "Relaxed Running" server (479 pk3s, 1.5GB). Ensure that you running release build to achieve maximum performance. Rust debug builds are slow like python/ruby.

## How it works

The (somewhat simplified) data flow is represented on the diagram below:

![diagram](workflow.png)

## Command line options

```
$ cargo run --bin generate_mapinfo -- --help
pk3rs 0.1.0
Nick Savchenko <nick@teichisma.info>
Generate mapinfo and apply overrides

USAGE:
    generate_mapinfo [FLAGS] [OPTIONS] --dest-dir <mapinfo_dir> --source-dir <pk3_dir>

FLAGS:
    -h, --help                     Prints help information
    -i, --ignore-dist-mapinfo      if set, don't read the mapinfo included in the PK3 file
    -x, --override-dist-mapinfo    if set, apply overrides even if a mapinfo file is already included in the PK3 file
    -V, --version                  Prints version information

OPTIONS:
    -d, --dest-dir <mapinfo_dir>    directory to which the emitted mapinfo files will be written
    -o, --overrides <overrides>     path to TOML file with overrides
    -s, --source-dir <pk3_dir>      directory containing pk3 files
```

## Overrides

Overrides are primarily intended for managing defrag maps, but can be used for other maps too. The idea is to place all custom options for all maps into a single file using a dense format (like packer does at his [package db](https://gitlab.com/packer/server-cts-data/tree/master/packages_db)).

TODO: describe the overrides format

