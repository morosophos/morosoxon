use crate::mapinfo::MapInfo;

pub trait GameTypeParams {
    fn id(&self) -> &'static str;
    fn generate_mapinfo(&self, _mapinfo: &mut MapInfo, _classname: &str) {}
    fn is_always_supported(&self, _spawnpoints: u16, _diameter: f64) -> bool {
        false
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum GameType {
    DM,
    LMS,
    RC,
    CTS,
    TDM,
    CTF,
    CA,
    DOM,
    KH,
    AS,
    ONS,
    NB,
    FT,
    KA,
    INV,
    Duel,
    BR,
}

pub const ALL_GAMETYPES: [GameType; 17] = [
    GameType::DM,
    GameType::LMS,
    GameType::RC,
    GameType::CTS,
    GameType::TDM,
    GameType::CTF,
    GameType::CA,
    GameType::DOM,
    GameType::KH,
    GameType::AS,
    GameType::ONS,
    GameType::NB,
    GameType::FT,
    GameType::KA,
    GameType::INV,
    GameType::Duel,
    GameType::BR,
];

pub const TWO_BASE_GAMETYPES: [GameType; 4] =
    [GameType::RC, GameType::CTF, GameType::AS, GameType::NB];

impl GameType {
    pub fn from_str(s: &str) -> Option<Self> {
        // Inefficient, but gotta reduce boilerplate before I learn how to use macroses
        for i in ALL_GAMETYPES.iter() {
            if i.get_params().id() == s {
                return Some(*i);
            }
        }
        None
    }
    pub fn get_params(self) -> Box<dyn GameTypeParams> {
        match self {
            GameType::DM => Box::new(DM {}),
            GameType::LMS => Box::new(LMS {}),
            GameType::RC => Box::new(RC {}),
            GameType::CTS => Box::new(CTS {}),
            GameType::TDM => Box::new(TDM {}),
            GameType::CTF => Box::new(CTF {}),
            GameType::CA => Box::new(CA {}),
            GameType::DOM => Box::new(DOM {}),
            GameType::KH => Box::new(KH {}),
            GameType::AS => Box::new(AS {}),
            GameType::ONS => Box::new(ONS {}),
            GameType::NB => Box::new(NB {}),
            GameType::FT => Box::new(FT {}),
            GameType::KA => Box::new(KA {}),
            GameType::INV => Box::new(INV {}),
            GameType::Duel => Box::new(Duel {}),
            GameType::BR => Box::new(BR {}),
        }
    }
}

pub struct DM;
pub struct LMS;
pub struct RC;
pub struct CTS;
pub struct TDM;
pub struct CTF;
pub struct CA;
pub struct DOM;
pub struct KH;
pub struct AS;
pub struct ONS;
pub struct NB;
pub struct FT;
pub struct KA;
pub struct INV;
pub struct Duel;
pub struct BR;

impl GameTypeParams for DM {
    fn id(&self) -> &'static str {
        "dm"
    }
    fn is_always_supported(&self, spawnpoints: u16, _diameter: f64) -> bool {
        spawnpoints > 1
    }
}

impl GameTypeParams for LMS {
    fn id(&self) -> &'static str {
        "lms"
    }
    fn is_always_supported(&self, spawnpoints: u16, _diameter: f64) -> bool {
        spawnpoints > 1
    }
}

impl GameTypeParams for RC {
    fn id(&self) -> &'static str {
        "rc"
    }

    fn generate_mapinfo(&self, mapinfo: &mut MapInfo, classname: &str) {
        if classname == "trigger_race_checkpoint" {
            mapinfo.gametypes.insert(GameType::RC);
        }
    }
}

impl GameTypeParams for CTS {
    fn id(&self) -> &'static str {
        "cts"
    }

    fn generate_mapinfo(&self, mapinfo: &mut MapInfo, classname: &str) {
        if classname == "target_startTimer" {
            mapinfo.gametypes.insert(GameType::CTS);
        }
    }
}

impl GameTypeParams for TDM {
    fn id(&self) -> &'static str {
        "tdm"
    }

    fn is_always_supported(&self, spawnpoints: u16, diameter: f64) -> bool {
        (spawnpoints >= 8) && (diameter > 4096.)
    }
}

impl GameTypeParams for CTF {
    fn id(&self) -> &'static str {
        "ctf"
    }

    fn generate_mapinfo(&self, mapinfo: &mut MapInfo, classname: &str) {
        // TODO: check that there are two flags, need to have access to all entities for that...
        if (classname == "item_flag_team2") || (classname == "team_CTF_blueflag") {
            mapinfo.gametypes.insert(GameType::CTF);
        }
    }
}

impl GameTypeParams for CA {
    fn id(&self) -> &'static str {
        "ca"
    }

    fn is_always_supported(&self, spawnpoints: u16, diameter: f64) -> bool {
        (spawnpoints >= 8) && (diameter > 4096.)
    }
}

impl GameTypeParams for DOM {
    fn id(&self) -> &'static str {
        "dom"
    }

    fn generate_mapinfo(&self, mapinfo: &mut MapInfo, classname: &str) {
        if classname == "dom_controlpoint" {
            mapinfo.gametypes.insert(GameType::DOM);
        }
    }
}

impl GameTypeParams for KH {
    fn id(&self) -> &'static str {
        "kh"
    }

    fn is_always_supported(&self, spawnpoints: u16, diameter: f64) -> bool {
        (spawnpoints >= 12) && (diameter > 5120.)
    }
}

impl GameTypeParams for AS {
    fn id(&self) -> &'static str {
        "as"
    }

    fn generate_mapinfo(&self, mapinfo: &mut MapInfo, classname: &str) {
        if classname == "target_assoult_rounded" {
            mapinfo.gametypes.insert(GameType::AS);
        }
    }
}

impl GameTypeParams for ONS {
    fn id(&self) -> &'static str {
        "ons"
    }

    fn generate_mapinfo(&self, mapinfo: &mut MapInfo, classname: &str) {
        if classname == "onslaught_generator" {
            mapinfo.gametypes.insert(GameType::ONS);
        }
    }
}

impl GameTypeParams for NB {
    fn id(&self) -> &'static str {
        "nb"
    }

    fn generate_mapinfo(&self, mapinfo: &mut MapInfo, classname: &str) {
        if classname.starts_with("nexball_") || classname.starts_with("ball") {
            mapinfo.gametypes.insert(GameType::NB);
        }
    }
}

impl GameTypeParams for FT {
    fn id(&self) -> &'static str {
        "ft"
    }

    fn is_always_supported(&self, spawnpoints: u16, diameter: f64) -> bool {
        (spawnpoints >= 8) && (diameter > 4096.)
    }
}

impl GameTypeParams for KA {
    fn id(&self) -> &'static str {
        "ka"
    }

    fn is_always_supported(&self, spawnpoints: u16, _diameter: f64) -> bool {
        spawnpoints > 1
    }
}

impl GameTypeParams for INV {
    fn id(&self) -> &'static str {
        "inv"
    }

    fn generate_mapinfo(&self, mapinfo: &mut MapInfo, classname: &str) {
        if classname == "invasion_spawnpoint" {
            mapinfo.gametypes.insert(GameType::NB);
        }
    }
}

impl GameTypeParams for Duel {
    fn id(&self) -> &'static str {
        "duel"
    }

    fn is_always_supported(&self, spawnpoints: u16, diameter: f64) -> bool {
        (spawnpoints > 1) && (diameter < 16384.)
    }
}

impl GameTypeParams for BR {
    fn id(&self) -> &'static str {
        "br"
    }

    fn is_always_supported(&self, spawnpoints: u16, diameter: f64) -> bool {
        (spawnpoints > 1) && (diameter < 16384.)
    }
}
