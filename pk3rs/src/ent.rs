use anyhow::{bail, Error};
use nom::{
    bytes::complete::{tag, take_till},
    character::complete::{multispace0, space0, space1},
    multi::{fold_many1, many1},
    sequence::{delimited, preceded, tuple},
    IResult,
};
use std::collections::HashMap;

fn double_quoted(i: &str) -> IResult<&str, &str> {
    delimited(tag("\""), take_till(|c| c == '"'), tag("\""))(i)
}

fn key_value(i: &str) -> IResult<&str, (String, String)> {
    let (input, (k, _, v, _)) = tuple((double_quoted, space1, double_quoted, space0))(i)?;
    Ok((input, (k.to_owned(), v.to_owned())))
}

fn key_value_list(i: &str) -> IResult<&str, HashMap<String, String>> {
    fold_many1(
        delimited(multispace0, key_value, multispace0),
        HashMap::new,
        |mut hm: HashMap<_, _>, item: (String, String)| {
            hm.insert(item.0, item.1);
            hm
        },
    )(i)
}

fn entity(i: &str) -> IResult<&str, HashMap<String, String>> {
    preceded(multispace0, delimited(tag("{"), key_value_list, tag("}")))(i)
}

fn entities(i: &str) -> IResult<&str, Vec<HashMap<String, String>>> {
    many1(entity)(i)
}

pub type Entity = HashMap<String, String>;

pub fn parse_entities(s: &str) -> Result<Vec<Entity>, Error> {
    match entities(s) {
        Ok((_, ents)) => Ok(ents),
        Err(e) => bail!("Can't parse entities: {}", e),
    }
}
