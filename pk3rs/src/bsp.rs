use super::map_tags::MapTags;
use crate::ent::parse_entities;
use anyhow::{bail, Error};
use byteorder::{LittleEndian, ReadBytesExt};
use std::collections::HashMap;
use std::io::{Read, Seek, SeekFrom};

const MAGIC: [u8; 4] = [73, 66, 83, 80]; // IBSP
const VERSION: i32 = 0x2e;
const HEADER_LUMPS: usize = 15;
// const HEADER_SIZE: u64 = 128; // 4 + 4 + HEADER_LUMPS * (4 + 4)
const LUMP_ENTITIES: usize = 0;
const LUMP_TEXTURES: usize = 1;
const SIZE_OF_TEXTURE: usize = 72;
const SURFACE_SLICK: i32 = 2;

#[derive(Debug)]
struct Texture {
    flags: i32,
}

#[derive(Debug)]
pub struct BSP {
    // direntries: [(i32, i32); HEADER_LUMPS],
    pub tags: MapTags,
    pub entities: Vec<HashMap<String, String>>,
}

impl BSP {
    pub fn new<T: Read + Seek>(f: &mut T) -> Result<Self, Error> {
        let mut magic = [0; 4];
        let n = f.read(&mut magic)?;
        if (n != 4) || (magic != MAGIC) {
            bail!("BSP Parse Error: Invalid magic")
        }
        let version = f.read_i32::<LittleEndian>()?;
        if version != VERSION {
            bail!("BSP Parse Error: Invalid version {}", version);
        }
        let mut direntries = [(0, 0); HEADER_LUMPS];
        for entry in direntries.iter_mut() {
            let offset = f.read_i32::<LittleEndian>()?;
            let length = f.read_i32::<LittleEndian>()?;
            entry.0 = offset;
            entry.1 = length;
        }

        f.seek(SeekFrom::Start(direntries[LUMP_ENTITIES].0 as u64))?;

        let mut entities: Vec<u8> = vec![0; direntries[LUMP_ENTITIES].1 as usize];
        f.read_exact(&mut entities)?;

        f.seek(SeekFrom::Start(direntries[LUMP_TEXTURES].0 as u64))?;
        let number_of_textures = (direntries[LUMP_TEXTURES].1 as usize) / SIZE_OF_TEXTURE;
        let mut textures = Vec::new();
        for _ in 0..number_of_textures {
            let mut name = [0; 64];
            f.read_exact(&mut name)?;
            let flags = f.read_i32::<LittleEndian>()?;
            let _contents = f.read_i32::<LittleEndian>()?;
            let mut nulpos = 0;
            while (name[nulpos] != 0) && (nulpos < 64) {
                nulpos += 1;
            }
            textures.push(Texture {
                // name: String::from_utf8_lossy(&name[..nulpos]).into_owned(),
                flags,
                // contents,
            });
        }
        let mut tags = MapTags::empty();

        for i in &textures {
            if i.flags & SURFACE_SLICK > 0 {
                tags |= MapTags::SLICK;
            }
        }

        let entities = parse_entities(&String::from_utf8_lossy(&entities))?;

        tags |= MapTags::from_ents(&entities);

        Ok(Self { tags, entities })
    }

    pub fn into_entities(self) -> Vec<HashMap<String, String>> {
        self.entities
    }
}
