use super::bsp::BSP;
use super::ent::Entity;
use super::game_type::GameType;
use super::map_tags::MapTags;
use super::mapinfo::MapInfo;
use super::overrides_config::OverridesConfig;
use super::{Arena, Defi};

pub struct Map {
    pub name: String,
    bsp: BSP,
    custom_ent: Option<Vec<Entity>>,
    mapinfo: Option<MapInfo>,
    arena: Option<Arena>,
    defi: Option<Defi>,
    levelshot: Option<image::DynamicImage>,
}

impl Map {
    pub fn new(
        name: String,
        bsp: BSP,
        custom_ent: Option<Vec<Entity>>,
        mapinfo: Option<MapInfo>,
        arena: Option<Arena>,
        defi: Option<Defi>,
        levelshot: Option<image::DynamicImage>,
    ) -> Self {
        Self {
            name,
            bsp,
            custom_ent,
            mapinfo,
            arena,
            defi,
            levelshot,
        }
    }

    pub fn has_arena(&self) -> bool {
        self.arena.is_some()
    }

    pub fn has_defi(&self) -> bool {
        self.defi.is_some()
    }

    pub fn override_ent(&mut self, new_ent: Vec<Entity>) {
        self.custom_ent = Some(new_ent)
    }

    pub fn get_ent(&self) -> &[Entity] {
        match self.custom_ent.as_ref() {
            Some(ent) => ent,
            None => &self.bsp.entities,
        }
    }

    pub fn override_mapinfo(&mut self, new_mapinfo: MapInfo) {
        self.mapinfo = Some(new_mapinfo)
    }

    pub fn maybe_generate_mapinfo(
        &self,
        overrides: Option<&OverridesConfig>,
        override_dist_mapinfo: bool,
        ignore_dist_mapinfo: bool,
    ) -> Option<MapInfo> {
        if self.mapinfo.is_some() && (!override_dist_mapinfo) {
            // Already have mapinfo and the caller opted not to override it, kaputt
            return None;
        }
        let mut gotta_return = false;
        let mut mapinfo = match (self.mapinfo.clone(), ignore_dist_mapinfo) {
            (Some(m), false) => m,
            (_, _) => {
                gotta_return = true;
                MapInfo::generate(self)
            }
        };
        if let Some(o) = overrides {
            if o.apply(&self.name, &mut mapinfo) {
                gotta_return = true;
            }
        }
        if gotta_return {
            Some(mapinfo)
        } else {
            None
        }
    }

    pub fn get_tags(&self, game_type: GameType) -> MapTags {
        let mut tags = self.bsp.tags;
        if let Some(mapinfo) = self.mapinfo.as_ref() {
            tags |= MapTags::from_mapinfo(mapinfo, game_type);
        }
        tags
    }

    pub fn get_levelshot(&self) -> Option<&image::DynamicImage> {
        self.levelshot.as_ref()
    }
}
