use crate::game_type::GameType;
use crate::mapinfo::{MapFeatures, MapFlags, MapInfo, SetTempForType};
use crate::mapinfo_macros::{macro_from_str, MapinfoMacro};
use anyhow::{bail, Error};
use fancy_regex::Regex;
use serde::Deserialize;
use std::cmp::{Eq, PartialEq};
use std::hash::{Hash, Hasher};
use indexmap::IndexMap;

#[derive(Debug)]
struct ReKey(Regex);

impl Hash for ReKey {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let s = self.0.as_str();
        s.hash(state);
    }
}

impl PartialEq for ReKey {
    fn eq(&self, other: &ReKey) -> bool {
        self.0.as_str() == other.0.as_str()
    }
}

impl Eq for ReKey {}

#[derive(Deserialize, Debug, Clone)]
pub struct OverridesEntryRaw {
    title: Option<String>,
    description: Option<String>,
    author: Option<String>,
    add_gametypes: Option<Vec<String>>,
    gametypes: Option<Vec<String>>,
    rm_gametypes: Option<Vec<String>>,
    weapons: Option<bool>,
    vehicles: Option<bool>,
    turrets: Option<bool>,
    monsters: Option<bool>,
    hidden: Option<bool>,
    forbidden: Option<bool>,
    frustrating: Option<bool>,
    noautomaplist: Option<bool>,
    size: Option<(u64, u64, u64, u64, u64, u64)>,
    fog: Option<String>,
    cdtrack: Option<String>,
    set_temps: Option<Vec<(String, String, String)>>,
    client_set_temps: Option<Vec<(String, String, String)>>,
    macros: Option<Vec<String>>,
}

pub struct OverridesEntry {
    title: Option<String>,
    description: Option<String>,
    author: Option<String>,
    // TODO: maybe parse gametype list here?
    add_gametypes: Option<Vec<String>>,
    gametypes: Option<Vec<String>>,
    rm_gametypes: Option<Vec<String>>,
    weapons: Option<bool>,
    vehicles: Option<bool>,
    turrets: Option<bool>,
    monsters: Option<bool>,
    hidden: Option<bool>,
    forbidden: Option<bool>,
    frustrating: Option<bool>,
    noautomaplist: Option<bool>,
    size: Option<(u64, u64, u64, u64, u64, u64)>,
    fog: Option<String>,
    cdtrack: Option<String>,
    set_temps: Vec<SetTempForType>,
    client_set_temps: Vec<SetTempForType>,
    macros: Vec<Box<dyn MapinfoMacro>>,
}

impl OverridesEntryRaw {
    fn get_set_temps(&self, client: bool) -> Result<Vec<SetTempForType>, Error> {
        let mut res = Vec::new();
        let raw = if client {
            &self.client_set_temps
        } else {
            &self.set_temps
        };
        if let Some(ref r) = raw {
            for i in r.iter() {
                let gt = if i.0 == "all" {
                    None
                } else if let Some(sgt) = GameType::from_str(&(i.0)) {
                    Some(sgt)
                } else {
                    bail!("Wrong game type: {}", i.0)
                };
                res.push(SetTempForType::new(gt, &i.1, &i.2));
            }
        };
        Ok(res)
    }

    fn get_macros(&self) -> Result<Vec<Box<dyn MapinfoMacro>>, Error> {
        let mut res = Vec::new();
        if let Some(ref raw) = self.macros {
            for i in raw.iter() {
                if let Some(m) = macro_from_str(i) {
                    res.push(m)
                } else {
                    bail!("Invalid macro: {}", i)
                }
            }
        }
        Ok(res)
    }
}

impl OverridesEntry {
    fn from_raw(e: OverridesEntryRaw) -> Result<Self, Error> {
        Ok(Self {
            set_temps: e.get_set_temps(false)?,
            client_set_temps: e.get_set_temps(true)?,
            macros: e.get_macros()?,
            title: e.title,
            description: e.description,
            author: e.author,
            add_gametypes: e.add_gametypes,
            gametypes: e.gametypes,
            rm_gametypes: e.rm_gametypes,
            weapons: e.weapons,
            vehicles: e.vehicles,
            turrets: e.turrets,
            monsters: e.monsters,
            hidden: e.hidden,
            forbidden: e.forbidden,
            frustrating: e.frustrating,
            noautomaplist: e.noautomaplist,
            size: e.size,
            fog: e.fog,
            cdtrack: e.cdtrack,
        })
    }

    fn apply(&self, mapinfo: &mut MapInfo) {
        // TODO: reduce boilerplate with macros
        if let Some(ref v) = self.title {
            mapinfo.title = v.clone()
        }
        if let Some(ref v) = self.description {
            mapinfo.description = v.clone()
        }
        if let Some(ref v) = self.author {
            mapinfo.author = v.clone()
        }
        if let Some(ref v) = self.add_gametypes {
            for gt_str in v.iter() {
                if let Some(gt) = GameType::from_str(gt_str) {
                    mapinfo.gametypes.insert(gt);
                }
            }
        }
        if let Some(ref v) = self.gametypes {
            mapinfo.gametypes = std::collections::HashSet::new();
            for gt_str in v.iter() {
                if let Some(gt) = GameType::from_str(gt_str) {
                    mapinfo.gametypes.insert(gt);
                }
            }
        }
        if let Some(ref v) = self.rm_gametypes {
            for gt_str in v.iter() {
                if let Some(gt) = GameType::from_str(gt_str) {
                    mapinfo.gametypes.remove(&gt);
                }
            }
        }
        if let Some(ref v) = self.fog {
            mapinfo.fog = Some(v.clone())
        }
        if let Some(ref v) = self.cdtrack {
            mapinfo.cdtrack = v.clone()
        }
        if let Some(v) = self.weapons {
            mapinfo.features.set(MapFeatures::WEAPONS, v)
        }
        if let Some(v) = self.vehicles {
            mapinfo.features.set(MapFeatures::VEHICLES, v)
        }
        if let Some(v) = self.turrets {
            mapinfo.features.set(MapFeatures::TURRETS, v)
        }
        if let Some(v) = self.monsters {
            mapinfo.features.set(MapFeatures::MONSTERS, v)
        }
        if let Some(v) = self.hidden {
            mapinfo.flags.set(MapFlags::HIDDEN, v)
        }
        if let Some(v) = self.forbidden {
            mapinfo.flags.set(MapFlags::FORBIDDEN, v)
        }
        if let Some(v) = self.frustrating {
            mapinfo.flags.set(MapFlags::FRUSTRATING, v)
        }
        if let Some(v) = self.noautomaplist {
            mapinfo.flags.set(MapFlags::NOAUTOMAPLIST, v)
        }

        if let Some((xmin, ymin, zmin, xmax, ymax, zmax)) = self.size {
            mapinfo.mins = (xmin, ymin, zmin);
            mapinfo.maxs = (xmax, ymax, zmax);
        }
        for i in &self.client_set_temps {
            mapinfo.apply_set_temp(i, true)
        }
        for i in &self.set_temps {
            mapinfo.apply_set_temp(i, false)
        }
        for i in &self.macros {
            i.apply(mapinfo)
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct OverridesConfigRaw {
    global: Option<OverridesEntryRaw>,
    m: IndexMap<String, OverridesEntryRaw>,
    macros: IndexMap<String, Vec<String>>,
}

pub struct OverridesConfig {
    global: Option<OverridesEntry>,
    m: IndexMap<ReKey, OverridesEntry>,
    macros: IndexMap<ReKey, Vec<Box<dyn MapinfoMacro>>>,
}

impl OverridesConfig {
    pub fn from_str(s: &str) -> Result<Self, Error> {
        let c: OverridesConfigRaw = toml::from_str(s)?;
        Self::from_raw(&c)
    }

    pub fn from_raw(cfg: &OverridesConfigRaw) -> Result<Self, Error> {
        // TODO: remove cloning
        let global = match cfg.global {
            Some(ref g) => Some(OverridesEntry::from_raw(g.clone())?),
            None => None,
        };
        let mut res = Self {
            global,
            m: IndexMap::new(),
            macros: IndexMap::new(),
        };
        for (k, v) in cfg.m.iter() {
            let new_k = ReKey(Regex::new(&format!("^(?i){}$", k))?);
            res.m.insert(new_k, OverridesEntry::from_raw(v.clone())?);
        }
        for (k, v) in cfg.macros.iter() {
            let new_k = ReKey(Regex::new(&format!("^(?i){}$", k))?);
            let mut new_v = Vec::new();
            for i in v.iter() {
                if let Some(m) = macro_from_str(i) {
                    new_v.push(m)
                } else {
                    bail!("Invalid macro: {}", i)
                }
            }
            res.macros.insert(new_k, new_v);
        }
        Ok(res)
    }

    pub fn apply(&self, name: &str, mapinfo: &mut MapInfo) -> bool {
        let mut applied = false;
        if let Some(ref global) = self.global {
            global.apply(mapinfo);
            applied = true;
        }
        for (k, v) in self.m.iter() {
            if let Ok(mr) = k.0.is_match(name) {
                if mr {
                    v.apply(mapinfo);
                    applied = true
                }
            }
        }
        for (k, v) in self.macros.iter() {
            if let Ok(mr) = k.0.is_match(name) {
                if mr {
                    for mac in v.iter() {
                        mac.apply(mapinfo);
                        applied = true;
                    }
                }
            }
        }
        applied
    }
}
