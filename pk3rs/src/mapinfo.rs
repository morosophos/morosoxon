use crate::game_type::{GameType, ALL_GAMETYPES, TWO_BASE_GAMETYPES};
use crate::map::Map;
use crate::mapinfo_parse::MapInfoLine;
use bitflags::bitflags;
use crc::{Crc, CRC_32_ISCSI};
use std::borrow::Borrow;
use std::cmp::{max, min, Eq};
use std::collections::{HashMap, HashSet};
use std::default::Default;
use std::hash::Hash;

pub const CASTAGNOLI: Crc<u32> = Crc::<u32>::new(&CRC_32_ISCSI);

bitflags! {
    #[derive(Debug, Clone)]
    pub struct MapFeatures: u8 {
        const WEAPONS = 1;
        const VEHICLES = 2;
        const TURRETS = 4;
        const MONSTERS = 8;
    }
}

bitflags! {
    #[derive(Debug, Clone)]
    pub struct MapFlags: u8 {
        const HIDDEN = 1;
        const FORBIDDEN = 2;
        const FRUSTRATING = 4;
        const NOAUTOMAPLIST = 8;
    }
}

#[derive(Debug, Clone)]
pub struct SetTempForType {
    pub gametype: Option<GameType>, // None -> all gametypes
    pub cvar: String,
    pub value: String,
}

impl SetTempForType {
    pub fn new(gametype: Option<GameType>, cvar: &str, value: &str) -> Self {
        Self {
            gametype,
            cvar: cvar.to_owned(),
            value: value.to_owned(),
        }
    }

    pub fn to_mapinfo_str(&self) -> String {
        let gt;
        if let Some(ref t) = self.gametype {
            gt = t.get_params().id();
        } else {
            gt = "all";
        }
        format!("{} {} {}", gt, self.cvar, self.value)
    }
}

#[derive(Debug, Clone)]
pub struct MapInfo {
    pub title: String,
    pub description: String,
    pub author: String,
    pub gametypes: HashSet<GameType>,
    pub features: MapFeatures,
    pub flags: MapFlags,
    pub gameversion_min: Option<u16>,
    pub mins: (u64, u64, u64),
    pub maxs: (u64, u64, u64),
    pub fog: Option<String>,
    pub cdtrack: String,
    pub set_temps: Vec<SetTempForType>,
    pub client_set_temps: Vec<SetTempForType>,
}

impl Default for MapInfo {
    fn default() -> Self {
        Self {
            title: String::new(),
            description: String::new(),
            author: String::new(),
            features: MapFeatures::empty(),
            flags: MapFlags::empty(),
            gameversion_min: None,
            gametypes: HashSet::new(),
            mins: (0, 0, 0),
            maxs: (0, 0, 0),
            fog: None,
            cdtrack: String::new(),
            set_temps: Vec::new(),
            client_set_temps: Vec::new(),
        }
    }
}

fn hm_val_eq<Q: Hash + Eq + ?Sized, T: Hash + Eq + Borrow<Q>>(
    hm: &HashMap<T, String>,
    key: &Q,
    expected_value: &str,
) -> bool {
    let val = hm.get(key).map(String::as_str);
    val == Some(expected_value)
}

pub fn parse_origin(s: &str) -> Option<(u64, u64, u64)> {
    let mut splitted = s.split(' ');
    let x = splitted.next()?.parse().ok()?;
    let y = splitted.next()?.parse().ok()?;
    let z = splitted.next()?.parse().ok()?;
    Some((x, y, z))
}

fn distance(p1: (u64, u64, u64), p2: (u64, u64, u64)) -> f64 {
    let diff = (
        (p1.0 - p2.0) as f64,
        (p1.1 - p2.1) as f64,
        (p1.2 - p2.2) as f64,
    );
    (diff.0.powi(2) + diff.1.powi(2) + diff.2.powi(2)).sqrt()
}

impl MapInfo {
    pub fn parse(s: &str) -> Result<Self, anyhow::Error> {
        let mut mapinfo: MapInfo = Default::default();
        for line in s.lines() {
            let parsed_line = MapInfoLine::parse_line(line);
            match parsed_line {
                MapInfoLine::Title(s) => mapinfo.title = s.to_string(),
                MapInfoLine::Author(s) => mapinfo.author = s.to_string(),
                MapInfoLine::Description(s) => mapinfo.description = s.to_string(),
                MapInfoLine::Feature(s) => {
                    let feature = match s {
                        "weapons" => Some(MapFeatures::WEAPONS),
                        "turrets" => Some(MapFeatures::TURRETS),
                        "vehicles" => Some(MapFeatures::VEHICLES),
                        "monsters" => Some(MapFeatures::MONSTERS),
                        _ => {
                            // shouldn't happen, but gotta include to please the type checker
                            println!("Warning: unknown feature {}", s);
                            None
                        }
                    };
                    if let Some(f) = feature {
                        mapinfo.features.insert(f);
                    }
                }
                MapInfoLine::Flag(s) => {
                    let flag = match s {
                        "hidden" => Some(MapFlags::HIDDEN),
                        "forbidden" => Some(MapFlags::FORBIDDEN),
                        "frustrating" => Some(MapFlags::FRUSTRATING),
                        "noautomaplist" => Some(MapFlags::NOAUTOMAPLIST),
                        _ => {
                            // shouldn't happen, but gotta include to please the type checker
                            println!("Warning: unknown flag {}", s);
                            None
                        }
                    };
                    if let Some(f) = flag {
                        mapinfo.flags.insert(f);
                    }
                }
                MapInfoLine::GameType(s) => {
                    let maybe_gt = GameType::from_str(s);
                    if let Some(gt) = maybe_gt {
                        mapinfo.gametypes.insert(gt);
                    }
                }
                MapInfoLine::Size(xmin, ymin, zmin, xmax, ymax, zmax) => {
                    mapinfo.mins = (xmin, ymin, zmin);
                    mapinfo.maxs = (xmax, ymax, zmax);
                }
                MapInfoLine::Fog(s) => mapinfo.fog = Some(s.to_string()),
                MapInfoLine::SetTempForType(raw_gt, cvar, val) => {
                    let gt = GameType::from_str(raw_gt);
                    if (raw_gt == "all") || (gt.is_some()) {
                        let st = if raw_gt == "all" {
                            SetTempForType::new(None, cvar, val)
                        } else {
                            SetTempForType::new(gt, cvar, val)
                        };
                        mapinfo.set_temps.push(st);
                    }
                }
                MapInfoLine::ClientSetTempForType(raw_gt, cvar, val) => {
                    let gt = GameType::from_str(raw_gt);
                    if (raw_gt == "all") || (gt.is_some()) {
                        let st = if raw_gt == "all" {
                            SetTempForType::new(None, cvar, val)
                        } else {
                            SetTempForType::new(gt, cvar, val)
                        };
                        mapinfo.client_set_temps.push(st);
                    }
                }
                MapInfoLine::Cdtrack(s) => mapinfo.cdtrack = s.to_string(),
                MapInfoLine::Empty => {}
            }
        }
        Ok(mapinfo)
    }

    pub fn apply_set_temp(&mut self, s: &SetTempForType, client: bool) {
        let field = if client {
            &mut self.client_set_temps
        } else {
            &mut self.set_temps
        };
        let mut updated = false;
        for i in field.iter_mut() {
            if (i.gametype == s.gametype) && (i.cvar == s.cvar) {
                i.value = s.value.clone();
                updated = true;
            }
        }
        if !updated {
            field.push(s.clone());
        }
    }

    fn generate_for_gametype(&mut self, gametype: GameType, classname: &str) {
        gametype.get_params().generate_mapinfo(self, classname)
    }

    pub fn generate(map: &Map) -> Self {
        let entities = map.get_ent();
        let mut mapinfo: MapInfo = Default::default();
        let mut music = String::new();
        let mut spawnplaces = 0;
        let mut spawnpoints = 0;
        for entity in entities {
            if hm_val_eq(entity, "classname", "worldspawn") {
                if let Some(v) = entity.get("author") {
                    mapinfo.author = v.clone();
                }
                if let Some(v) = entity.get("_description") {
                    mapinfo.description = v.clone();
                }
                if let Some(v) = entity.get("noise") {
                    music = v.clone();
                }
                if let Some(v) = entity.get("music") {
                    music = v.clone();
                }
                if let Some(v) = entity.get("message") {
                    if let Some(i) = v.find(" by ") {
                        if mapinfo.author.is_empty() {
                            let mut v = v.clone();
                            let mut rest = v.split_off(i);
                            mapinfo.author = rest.split_off(4);
                            mapinfo.title = v;
                        }
                    } else {
                        mapinfo.title = v.clone();
                    }
                }
            } else {
                if let Some(v) = entity.get("origin") {
                    if let Some(orig) = parse_origin(v) {
                        mapinfo.mins.0 = min(mapinfo.mins.0, orig.0);
                        mapinfo.mins.1 = min(mapinfo.mins.1, orig.1);
                        mapinfo.mins.2 = min(mapinfo.mins.2, orig.2);
                        mapinfo.maxs.0 = max(mapinfo.maxs.0, orig.0);
                        mapinfo.maxs.1 = max(mapinfo.maxs.1, orig.1);
                        mapinfo.maxs.2 = max(mapinfo.maxs.2, orig.2);
                    }
                }
                if let Some(v) = entity.get("race_place") {
                    let rf: Result<f64, _> = v.parse();
                    if let Ok(f) = rf {
                        if f > 0. {
                            spawnplaces = 1
                        }
                    }
                }
                if let Some(v) = entity.get("classname") {
                    if (v == "info_player_team1")
                        || (v == "info_player_team2")
                        || (v == "info_player_start")
                        || (v == "info_player_deathmatch")
                    {
                        spawnpoints += 1
                    } else if (v == "weapon_nex") || (v == "weapon_railgun") {
                    } else if v.starts_with("weapon_") {
                        mapinfo.features |= MapFeatures::WEAPONS
                    } else if v.starts_with("turret_") {
                        mapinfo.features |= MapFeatures::TURRETS
                    } else if v.starts_with("vehicle_") {
                        mapinfo.features |= MapFeatures::VEHICLES
                    } else if v.starts_with("monster_") {
                        mapinfo.features |= MapFeatures::MONSTERS
                    } else if (v == "target_music") || (v == "trigger_music") {
                        music = String::new();
                    } else {
                        for i in &ALL_GAMETYPES {
                            mapinfo.generate_for_gametype(*i, v);
                        }
                    }
                }
            }
        } // end for entity in ..
        if !music.is_empty() {
            if music.ends_with(".wav") || music.ends_with(".ogg") {
                music.truncate(music.len() - 4);
            }
            mapinfo.cdtrack = music;
        } else {
            let c = CASTAGNOLI.checksum(map.name.as_bytes());
            let track = (c % 21) + 2;
            mapinfo.cdtrack = track.to_string();
        }
        let diameter = distance(mapinfo.maxs, mapinfo.mins);
        let mut symmetrical = false;
        for i in mapinfo.gametypes.iter() {
            for j in TWO_BASE_GAMETYPES.iter() {
                if i == j {
                    symmetrical = true
                }
            }
        }
        if !symmetrical {
            for i in ALL_GAMETYPES.iter() {
                if i.get_params().is_always_supported(spawnpoints, diameter) {
                    mapinfo.gametypes.insert(*i);
                }
            }
        }

        if mapinfo.gametypes.contains(&GameType::RC) && (spawnplaces == 0) {
            mapinfo.gametypes.remove(&GameType::RC);
            mapinfo.gametypes.insert(GameType::CTS);
        }
        if map.has_defi() {
            mapinfo
                .set_temps
                .push(SetTempForType::new(None, "sv_q3compat_changehitbox", "1"));
        }
        mapinfo
    }

    pub fn dump(&self) -> String {
        let mut s = String::from("// generated by pk3rs https://gitlab.com/morosophos/pk3rs\n\n");

        if !self.title.is_empty() {
            s.push_str(&format!("title {}\n", self.title));
        }
        if !self.description.is_empty() {
            s.push_str(&format!("description {}\n", self.description));
        }
        if !self.author.is_empty() {
            s.push_str(&format!("author {}\n", self.author));
        }
        let mut gametypes: Vec<_> = self.gametypes.iter().map(|x| x.get_params().id()).collect();
        gametypes.sort_unstable();
        for gt in gametypes.iter() {
            s.push_str(&format!("gametype {}\n", gt));
        }

        if self.flags.contains(MapFlags::HIDDEN) {
            s.push_str("hidden\n");
        }

        if self.flags.contains(MapFlags::FORBIDDEN) {
            s.push_str("forbidden\n");
        }

        if self.flags.contains(MapFlags::FRUSTRATING) {
            s.push_str("frustrating\n");
        }

        if self.flags.contains(MapFlags::NOAUTOMAPLIST) {
            s.push_str("noautomaplist\n");
        }

        if self.features.contains(MapFeatures::WEAPONS) {
            s.push_str("has weapons\n")
        }

        if self.features.contains(MapFeatures::TURRETS) {
            s.push_str("has turrets\n")
        }

        if self.features.contains(MapFeatures::VEHICLES) {
            s.push_str("has vehicles\n")
        }

        if self.features.contains(MapFeatures::MONSTERS) {
            s.push_str("has monsters\n")
        }

        if let Some(ver) = self.gameversion_min {
            s.push_str(&format!("gameversion_min {}\n", ver));
        }

        if (self.mins.0 != 0)
            || (self.mins.1 != 0)
            || (self.mins.2 != 0)
            || (self.maxs.0 != 0)
            || (self.maxs.1 != 0)
            || (self.maxs.2 != 0)
        {
            s.push_str(&format!(
                "size {} {} {} {} {} {}\n",
                self.mins.0, self.mins.1, self.mins.2, self.maxs.0, self.maxs.1, self.maxs.2
            ));
        }

        if let Some(ref fog) = self.fog {
            s.push_str(&format!("fog {}\n", fog));
        }

        if !self.cdtrack.is_empty() {
            s.push_str(&format!("cdtrack {}\n", self.cdtrack));
        }

        for i in self.set_temps.iter() {
            s.push_str(&format!("settemp_for_type {}\n", i.to_mapinfo_str()))
        }

        for i in self.client_set_temps.iter() {
            s.push_str(&format!("clientsettemp_for_type {}\n", i.to_mapinfo_str()))
        }
        s
    }
}
