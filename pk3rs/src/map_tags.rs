use crate::game_type::GameType;
use crate::mapinfo::MapInfo;
use bitflags::bitflags;
use lazy_static::lazy_static;
use std::collections::HashMap;

bitflags! {
    #[derive(Clone, Debug, Copy)]
    pub struct MapTags: u32 {
        const HASTE      = 0b0000_0001;
        const SLICK      = 0b0000_0010;
        const VQ3_COMPAT = 0b0000_0100;
        const NEXRUN     = 0b0000_1000;
        const ROCKET     = 0b0001_0000;
        const MORTAR     = 0b0010_0000;
        const CRYLINK    = 0b0100_0000;
        const HAGAR      = 0b1000_0000;
        const HOOK       = 0b0000_0001_0000_0000;
        const FIREBALL   = 0b0000_0010_0000_0000;
        const NEX        = 0b0000_0100_0000_0000;
        const ELECTRO    = 0b0000_1000_0000_0000;
        const BLASTER    = 0b0001_0000_0000_0000;
        const MINE_LAYER = 0b0010_0000_0000_0000;
        const MG         = 0b0100_0000_0000_0000;
        const SG         = 0b1000_0000_0000_0000;
        const ALL_WEPS   = Self::ROCKET.bits() |
                           Self::MORTAR.bits() |
                           Self::CRYLINK.bits() |
                           Self::HAGAR.bits() |
                           Self::FIREBALL.bits() |
                           Self::NEX.bits();
    }
}

impl MapTags {
    pub fn from_mapinfo(mapinfo: &MapInfo, gt: GameType) -> Self {
        // TODO: maybe do it without string comparison, and store applied macro stack instead?
        // but it will be incompatible with manually edited mapinfo
        // but it will be incompatible anyways cause the "true" values are ranged from 1 to inf?
        let mut res = Self::empty();
        for i in &mapinfo.set_temps {
            if i.gametype.is_none() || (i.gametype == Some(gt)) {
                match (i.cvar.as_str(), i.value.as_str()) {
                    ("sv_friction", "1") => res |= Self::SLICK,
                    ("sv_maxspeed", "400") => res |= Self::NEXRUN,
                    ("sv_vq3compat", "1") => res |= Self::VQ3_COMPAT,
                    ("g_weaponarena", "1") => res |= Self::ALL_WEPS,
                    ("g_balance_hagar_weaponstartoverride", "1") => res |= Self::HAGAR,
                    ("g_balance_mortar_weaponstartoverride", "1") => res |= Self::MORTAR,
                    ("g_balance_devastator_weaponstartoverride", "1") => res |= Self::ROCKET,
                    ("g_balance_crylink_weaponstartoverride", "1") => res |= Self::CRYLINK,
                    (_, _) => {}
                }
            }
        }
        res
    }

    pub fn from_ents(ents: &[HashMap<String, String>]) -> Self {
        let mut res = Self::empty();
        lazy_static! {
            static ref HASHMAP: HashMap<&'static str, MapTags> = {
                let mut m = HashMap::new();
                m.insert("item_haste", MapTags::HASTE);
                // q3 entities
                m.insert("weapon_plasmagun", MapTags::HAGAR);
                m.insert("weapon_railgun", MapTags::NEX);
                m.insert("weapon_bfg", MapTags::CRYLINK);
                m.insert("weapon_grenadelauncher", MapTags::MORTAR);
                m.insert("weapon_rocketlauncher", MapTags::ROCKET);
                m.insert("weapon_grapplinghook", MapTags::HOOK);
                m.insert("weapon_prox_launcher", MapTags::MINE_LAYER);
                m.insert("weapon_lightning", MapTags::ELECTRO);
                // nexuiz entities
                m.insert("weapon_nex", MapTags::NEX);
                m.insert("weapon_laser", MapTags::BLASTER);
                m.insert("weapon_hook", MapTags::HOOK);
                m.insert("weapon_uzi", MapTags::MG);
                m.insert("weapon_shotgun", MapTags::SG);
                // xonotic entities
                m.insert("weapon_blaster", MapTags::BLASTER);
                m.insert("weapon_vortex", MapTags::NEX);
                m.insert("weapon_crylink", MapTags::CRYLINK);
                m.insert("weapon_electro", MapTags::ELECTRO);
                m.insert("weapon_hagar", MapTags::HAGAR);
                m.insert("weapon_mortar", MapTags::MORTAR);
                m.insert("weapon_devastator", MapTags::ROCKET);
                m.insert("weapon_fireball", MapTags::FIREBALL);
                m.insert("weapon_machinegun", MapTags::MG);
                m
            };
        }
        for ent in ents {
            if let Some(cn) = ent.get("classname") {
                if let Some(t) = HASHMAP.get(cn.as_str()) {
                    res |= *t;
                } else if let Some(netname) = ent.get("netname") {
                    for i in netname.split_ascii_whitespace() {
                        let wpn = format!("weapon_{}", i);
                        if let Some(t) = HASHMAP.get(wpn.as_str()) {
                            res |= *t;
                        }
                    }
                }
            }
        }

        res
    }
}
