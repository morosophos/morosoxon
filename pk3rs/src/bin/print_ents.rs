use anyhow::Error;
use pk3rs::PK3;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt()]
/// Print tags for a map
struct Opt {
    #[structopt(name = "PK3", parse(from_os_str))]
    /// path to pk3 file
    pk3_path: PathBuf,
}

fn main() -> Result<(), Error> {
    let opt = Opt::from_args();
    let pk3 = PK3::from_path(&opt.pk3_path)?;
    let maplist = pk3.into_maplist()?;

    for map in maplist {
        for ent in map.get_ent() {
            println!("{}: {:?}", map.name, ent);
        }
    }
    Ok(())
}
