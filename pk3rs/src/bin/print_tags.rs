use anyhow::Error;
use pk3rs::{GameType, MapInfo, PK3};
use std::fs::read_to_string;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt()]
/// Print tags for a map
struct Opt {
    #[structopt(short = "m", long = "mapinfo", parse(from_os_str))]
    /// optional path to custom mapinfo
    mapinfo_path: Option<PathBuf>,
    #[structopt(name = "PK3", parse(from_os_str))]
    /// path to pk3 file
    pk3_path: PathBuf,
}

fn main() -> Result<(), Error> {
    let opt = Opt::from_args();
    let mapinfo = match opt.mapinfo_path {
        Some(m) => {
            let mapinfo_data = read_to_string(&m)?;
            Some(MapInfo::parse(&mapinfo_data)?)
        }
        None => None,
    };
    let pk3 = PK3::from_path(&opt.pk3_path)?;
    let maplist = pk3.into_maplist()?;
    for mut map in maplist {
        if let Some(mi) = mapinfo.as_ref() {
            map.override_mapinfo(mi.clone());
        }
        let tags = map.get_tags(GameType::CTS);
        println!("{}: {:?}", map.name, tags);
    }
    Ok(())
}
