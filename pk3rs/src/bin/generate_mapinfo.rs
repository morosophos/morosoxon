use anyhow::{bail, Error};
use pk3rs::overrides_config::OverridesConfig;
use pk3rs::PK3;
use std::io::Read;
use std::path::PathBuf;
use structopt::StructOpt;
use walkdir::WalkDir;

#[derive(StructOpt, Debug)]
#[structopt()]
/// Generate mapinfo and apply overrides
struct Opt {
    #[structopt(short = "s", long = "source-dir", parse(from_os_str))]
    /// directory containing pk3 files
    pk3_dir: PathBuf,

    #[structopt(short = "d", long = "dest-dir", parse(from_os_str))]
    /// directory to which the emitted mapinfo files will be written
    mapinfo_dir: PathBuf,

    #[structopt(short = "o", long = "overrides", parse(from_os_str))]
    /// path to TOML file with overrides
    overrides: Option<PathBuf>,

    #[structopt(short = "x", long = "override-dist-mapinfo")]
    /// if set, apply overrides even if a mapinfo file is already included in the PK3 file
    override_dist_mapinfo: bool,

    #[structopt(short = "i", long = "ignore-dist-mapinfo")]
    /// if set, don't read the mapinfo included in the PK3 file
    ignore_dist_mapinfo: bool,
}

fn main() -> Result<(), Error> {
    let opt = Opt::from_args();
    if !opt.pk3_dir.is_dir() {
        bail!("ERROR: source directory doesn't exist or isn't a directory")
    }
    if !opt.mapinfo_dir.is_dir() {
        bail!("ERROR: mapinfo directory doesn't exist or isn't a directory")
    }
    let overrides_config = if let Some(ref overrides_path) = opt.overrides {
        let mut f = std::fs::File::open(overrides_path)?;
        let mut s = String::new();
        f.read_to_string(&mut s)?;
        Some(OverridesConfig::from_str(&s)?)
    } else {
        None
    };
    let walker = WalkDir::new(&opt.pk3_dir);
    for r_entry in walker {
        let entry = r_entry?;
        let path = entry.path();
        if let Some(ext) = path.extension() {
            if ext == "pk3" {
                let pk3 = PK3::from_path(&path)?;
                let maplist = pk3.into_maplist()?;
                for map in &maplist {
                    let maybe_mapinfo = map.maybe_generate_mapinfo(
                        overrides_config.as_ref(),
                        opt.override_dist_mapinfo,
                        opt.ignore_dist_mapinfo,
                    );
                    if let Some(mapinfo) = maybe_mapinfo {
                        let file_name = format!("{}.mapinfo", map.name);
                        let output_path = opt.mapinfo_dir.join(file_name);
                        std::fs::write(&output_path, &mapinfo.dump())?;
                    } else {
                        println!("FAILED TO GENERATE FOR MAP {}!", map.name);
                    }
                }
            }
        }
    }

    // dbg!(&raw);
    Ok(())
}
