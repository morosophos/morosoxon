use crate::mapinfo::{MapInfo, SetTempForType};

pub trait MapinfoMacro {
    fn id(&self) -> &'static str;
    fn apply(&self, mapinfo: &mut MapInfo);
    // fn deapply(&self, mapinfo: &mut MapInfo);
}

struct HasteMacro {}

impl MapinfoMacro for HasteMacro {
    fn id(&self) -> &'static str {
        "haste"
    }
    fn apply(&self, mapinfo: &mut MapInfo) {
        mapinfo.apply_set_temp(&SetTempForType::new(None, "sv_maxspeed", "400"), false);
        mapinfo.apply_set_temp(&SetTempForType::new(None, "sv_maxairspeed", "400"), false);
    }
    // fn deapply(&self, mapinfo: &mut MapInfo) {
    //     self.rm_set_temp(mapinfo, None, "sv_maxspeed");
    //     self.rm_set_temp(mapinfo, None, "sv_maxairspeed");
    // }
}

struct NoclipsMacro {}

impl MapinfoMacro for NoclipsMacro {
    fn id(&self) -> &'static str {
        "noclips"
    }
    fn apply(&self, mapinfo: &mut MapInfo) {
        mapinfo.apply_set_temp(
            &SetTempForType::new(None, "g_playerclip_collisions", "0"),
            false,
        )
    }
}

struct ClipsMacro {}

impl MapinfoMacro for ClipsMacro {
    fn id(&self) -> &'static str {
        "clips"
    }
    fn apply(&self, mapinfo: &mut MapInfo) {
        mapinfo.apply_set_temp(
            &SetTempForType::new(None, "g_playerclip_collisions", "1"),
            false,
        )
    }
}


struct RoxMacro {}

impl MapinfoMacro for RoxMacro {
    fn id(&self) -> &'static str {
        "rox"
    }

    fn apply(&self, mapinfo: &mut MapInfo) {
        mapinfo.apply_set_temp(
            &SetTempForType::new(None, "g_balance_devastator_weaponstartoverride", "1"),
            false,
        )
    }
}

struct BFGMacro {}

impl MapinfoMacro for BFGMacro {
    fn id(&self) -> &'static str {
        "bfg"
    }

    fn apply(&self, mapinfo: &mut MapInfo) {
        mapinfo.apply_set_temp(
            &SetTempForType::new(None, "g_balance_crylink_weaponstartoverride", "1"),
            false,
        )
    }
}

struct MortarMacro {}

impl MapinfoMacro for MortarMacro {
    fn id(&self) -> &'static str {
        "mortar"
    }

    fn apply(&self, mapinfo: &mut MapInfo) {
        mapinfo.apply_set_temp(
            &SetTempForType::new(None, "g_balance_mortar_weaponstartoverride", "1"),
            false,
        )
    }
}

struct HagarMacro {}

impl MapinfoMacro for HagarMacro {
    fn id(&self) -> &'static str {
        "hagar"
    }

    fn apply(&self, mapinfo: &mut MapInfo) {
        mapinfo.apply_set_temp(
            &SetTempForType::new(None, "g_balance_hagar_weaponstartoverride", "1"),
            false,
        )
    }
}

struct WarenaMacro {}

impl MapinfoMacro for WarenaMacro {
    fn id(&self) -> &'static str {
        "warena"
    }

    fn apply(&self, mapinfo: &mut MapInfo) {
        mapinfo.apply_set_temp(&SetTempForType::new(None, "g_weaponarena", "all"), false)
    }
}

struct SlickMacro {}

impl MapinfoMacro for SlickMacro {
    fn id(&self) -> &'static str {
        "slick"
    }

    fn apply(&self, mapinfo: &mut MapInfo) {
        mapinfo.apply_set_temp(&SetTempForType::new(None, "sv_friction", "0"), false)
    }
}

// IMO we don't need vq3size macro, will be automatically added to all maps ported from q3
// struct VQ3SizeMacro {}

fn all_macros() -> Vec<Box<dyn MapinfoMacro>> {
    vec![
        Box::new(HasteMacro {}),
        Box::new(NoclipsMacro {}),
        Box::new(ClipsMacro {}),
        Box::new(RoxMacro {}),
        Box::new(BFGMacro {}),
        Box::new(MortarMacro {}),
        Box::new(HagarMacro {}),
        Box::new(SlickMacro {}),
        Box::new(WarenaMacro {}),
    ]
}

pub fn macro_from_str(s: &str) -> Option<Box<dyn MapinfoMacro>> {
    for i in all_macros() {
        if i.id() == s {
            return Some(i);
        }
    }
    None
}
