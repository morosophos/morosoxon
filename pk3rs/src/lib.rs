pub mod bsp;
mod ent;
mod game_type;
mod map;
pub mod map_tags;
pub mod mapinfo;
pub mod mapinfo_macros;
pub mod mapinfo_parse;
pub mod overrides_config;

use bsp::BSP;
use ent::{parse_entities, Entity};
use anyhow::{bail, Error};
pub use game_type::GameType;
use map::Map;
pub use map_tags::MapTags;
pub use mapinfo::MapInfo;
use std::collections::HashMap;
use std::ffi::OsStr;
use std::fs::File;
use std::io::{BufReader, Cursor, Read, Seek};
use std::path::Path;
use zip::ZipArchive;

pub struct PK3 {
    bsp: HashMap<PK3FileId, PK3File<BSPParser>>,
    mapinfo: HashMap<PK3FileId, PK3File<MapInfoParser>>,
    ent: HashMap<PK3FileId, PK3File<EntParser>>,
    defi: HashMap<PK3FileId, PK3File<DefiParser>>,
    arena: HashMap<PK3FileId, PK3File<ArenaParser>>,
    levelshots: HashMap<PK3FileId, PK3File<ImageParser>>,
}

impl PK3 {
    fn new() -> Self {
        Self {
            bsp: HashMap::new(),
            mapinfo: HashMap::new(),
            ent: HashMap::new(),
            defi: HashMap::new(),
            arena: HashMap::new(),
            levelshots: HashMap::new(),
        }
    }

    pub fn from_path<T: AsRef<Path>>(path: &T) -> Result<Self, Error> {
        let f = File::open(path)?;
        let reader = BufReader::new(f);
        Self::from_archive(&mut ZipArchive::new(reader)?)
    }

    pub fn from_archive<R: Read + Seek>(archive: &mut ZipArchive<R>) -> Result<Self, Error> {
        let mut res = PK3::new();
        for i in 0..archive.len() {
            let mut file = archive.by_index(i)?;
            let name = file.mangled_name();
            let parent = name.parent();
            let file_stem = name.file_stem();
            let file_ext = name.extension().and_then(PK3FileExt::from_os_str);
            if let Some(d) = parent
                .map(Path::as_os_str)
                .and_then(OsStr::to_str)
                .map(str::to_ascii_lowercase)
            {
                if (d == "maps") || (d == "levelshots") {
                    if let (Some(map_name), Some(e)) = (file_stem, file_ext) {
                        let id = PK3FileId::from_os_str(map_name);
                        match e {
                            PK3FileExt::Jpeg | PK3FileExt::Png | PK3FileExt::Tga => {
                                let mut f = PK3File {
                                    ext: e,
                                    contents: Vec::new(),
                                    parser: ImageParser {},
                                };
                                file.read_to_end(&mut f.contents)?;
                                res.levelshots.insert(id, f);
                            }
                            _ => {}
                        }
                    }
                }
                if d == "maps" {
                    if let (Some(map_name), Some(e)) = (file_stem, file_ext) {
                        let id = PK3FileId::from_os_str(map_name);
                        match e {
                            PK3FileExt::Bsp => {
                                let mut f = PK3File {
                                    ext: e,
                                    contents: Vec::new(),
                                    parser: BSPParser {},
                                };
                                file.read_to_end(&mut f.contents)?;
                                res.bsp.insert(id, f);
                            }
                            PK3FileExt::Ent => {
                                let mut f = PK3File {
                                    ext: e,
                                    contents: Vec::new(),
                                    parser: EntParser {},
                                };
                                file.read_to_end(&mut f.contents)?;
                                res.ent.insert(id, f);
                            }
                            PK3FileExt::Mapinfo => {
                                let mut f = PK3File {
                                    ext: e,
                                    contents: Vec::new(),
                                    parser: MapInfoParser {},
                                };
                                file.read_to_end(&mut f.contents)?;
                                res.mapinfo.insert(id, f);
                            }
                            _ => {}
                        }
                    }
                }
                if d == "scripts" {
                    if let (Some(map_name), Some(e)) = (file_stem, file_ext) {
                        let id = PK3FileId::from_os_str(map_name);
                        match e {
                            PK3FileExt::Defi => {
                                let mut f = PK3File {
                                    ext: e,
                                    contents: Vec::new(),
                                    parser: DefiParser {},
                                };
                                file.read_to_end(&mut f.contents)?;
                                res.defi.insert(id, f);
                            }
                            PK3FileExt::Arena => {
                                let mut f = PK3File {
                                    ext: e,
                                    contents: Vec::new(),
                                    parser: ArenaParser {},
                                };
                                file.read_to_end(&mut f.contents)?;
                                res.arena.insert(id, f);
                            }
                            _ => {}
                        }
                    }
                }
            }
        }
        Ok(res)
    }
    pub fn into_maplist(self) -> Result<Vec<Map>, Error> {
        let mut res = Vec::new();
        for (map_name, bsp_file) in self.bsp.into_iter() {
            let mapinfo_file = self.mapinfo.get(&map_name);
            let ent_file = self.ent.get(&map_name);
            let defi = self.defi.get(&map_name);
            let arena = self.arena.get(&map_name);
            let levelshot = self.levelshots.get(&map_name);
            let parsed_mapinfo = match mapinfo_file {
                Some(m) => Some(m.parse()?),
                None => None,
            };
            let parsed_ent = match ent_file {
                Some(e) => Some(e.parse()?),
                None => None,
            };
            let parsed_arena = match arena {
                Some(a) => Some(a.parse()?),
                None => None,
            };
            let parsed_defi = match defi {
                Some(d) => Some(d.parse()?),
                None => None,
            };
            let parsed_levelshot = match levelshot {
                Some(l) => Some(l.parse()?),
                None => None,
            };
            res.push(Map::new(
                map_name.into_string(),
                bsp_file.parse()?,
                parsed_ent,
                parsed_mapinfo,
                parsed_arena,
                parsed_defi,
                parsed_levelshot,
            ));
        }
        Ok(res)
    }
}

#[derive(Debug, Clone, PartialEq, Hash)]
pub struct PK3FileId(String);

impl PK3FileId {
    fn from_os_str(s: &OsStr) -> Self {
        Self(s.to_string_lossy().into_owned())
    }

    pub fn into_string(self) -> String {
        self.0
    }
}

impl Eq for PK3FileId {}

#[derive(Debug, Clone, Copy)]
pub enum PK3FileExt {
    Defi,
    Arena,
    Bsp,
    Tga,
    Jpeg,
    Png,
    Mapinfo,
    Ent,
    Map,
}

impl PK3FileExt {
    pub fn from_os_str(s: &OsStr) -> Option<Self> {
        let lowcase = s.to_str().map(str::to_ascii_lowercase);
        if let Some(s) = lowcase {
            match s.as_str() {
                "defi" => Some(PK3FileExt::Defi),
                "arena" => Some(PK3FileExt::Arena),
                "bsp" => Some(PK3FileExt::Bsp),
                "tga" => Some(PK3FileExt::Tga),
                "png" => Some(PK3FileExt::Png),
                "mapinfo" => Some(PK3FileExt::Mapinfo),
                "jpeg" | "jpg" => Some(PK3FileExt::Jpeg),
                "ent" => Some(PK3FileExt::Ent),
                "map" => Some(PK3FileExt::Map),
                _ => None,
            }
        } else {
            None
        }
    }
}

pub struct PK3File<T: PK3FileParser> {
    // index: usize,
    // contents: PK3FileContents,
    ext: PK3FileExt,
    contents: Vec<u8>,
    parser: T,
}

impl<T: PK3FileParser> PK3File<T> {
    fn parse(&self) -> Result<T::ParseResult, Error> {
        self.parser.parse(self.ext, &self.contents)
    }
}

pub trait PK3FileParser {
    type ParseResult;
    fn parse(&self, ext: PK3FileExt, raw: &[u8]) -> Result<Self::ParseResult, Error>;
}

struct BSPParser {}

impl PK3FileParser for BSPParser {
    type ParseResult = BSP;
    fn parse(&self, _ext: PK3FileExt, raw: &[u8]) -> Result<Self::ParseResult, Error> {
        let mut cursor = Cursor::new(raw);
        BSP::new(&mut cursor)
    }
}

struct MapInfoParser {}

impl PK3FileParser for MapInfoParser {
    type ParseResult = MapInfo;
    fn parse(&self, _ext: PK3FileExt, raw: &[u8]) -> Result<Self::ParseResult, Error> {
        let s = String::from_utf8_lossy(raw);
        MapInfo::parse(&s)
    }
}

struct EntParser {}

impl PK3FileParser for EntParser {
    type ParseResult = Vec<Entity>;
    fn parse(&self, _ext: PK3FileExt, raw: &[u8]) -> Result<Self::ParseResult, Error> {
        let s = String::from_utf8_lossy(raw);
        parse_entities(&s)
    }
}

// we don't care about contents of .defi and .arena atm
#[derive(Clone, Debug, Copy)]
pub struct Defi {}
struct DefiParser {}

impl PK3FileParser for DefiParser {
    type ParseResult = Defi;
    fn parse(&self, _ext: PK3FileExt, _raw: &[u8]) -> Result<Self::ParseResult, Error> {
        Ok(Defi {})
    }
}

#[derive(Clone, Debug, Copy)]
pub struct Arena {}
struct ArenaParser {}

impl PK3FileParser for ArenaParser {
    type ParseResult = Arena;

    fn parse(&self, _ext: PK3FileExt, _raw: &[u8]) -> Result<Self::ParseResult, Error> {
        Ok(Arena {})
    }
}

struct ImageParser {}

impl PK3FileParser for ImageParser {
    type ParseResult = image::DynamicImage;

    fn parse(&self, ext: PK3FileExt, raw: &[u8]) -> Result<Self::ParseResult, Error> {
        let format = match ext {
            PK3FileExt::Png => Some(image::ImageFormat::Png),
            PK3FileExt::Jpeg => Some(image::ImageFormat::Jpeg),
            PK3FileExt::Tga => Some(image::ImageFormat::Tga),
            _ => None,
        };
        let cursor = Cursor::new(raw);
        match format {
            Some(f) => Ok(image::load(cursor, f)?),
            None => bail!("invalid image extension: {:?}", ext),
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
