use nom::{
    branch::alt,
    bytes::complete::{tag, take_till},
    character::complete::{digit1, not_line_ending, space1},
    sequence::{delimited, preceded, tuple},
    IResult,
};

#[derive(Debug, PartialEq)]
pub enum MapInfoLine<'a> {
    Title(&'a str),
    Author(&'a str),
    Description(&'a str),
    Feature(&'a str),
    Flag(&'a str),
    GameType(&'a str),
    Size(u64, u64, u64, u64, u64, u64),
    Fog(&'a str),
    SetTempForType(&'a str, &'a str, &'a str),
    ClientSetTempForType(&'a str, &'a str, &'a str),
    Cdtrack(&'a str),
    Empty,
}

impl<'a> MapInfoLine<'a> {
    pub fn parse_line(s: &'a str) -> Self {
        if s.starts_with('#') || s.starts_with('_') {
            return MapInfoLine::Empty;
        }
        let s1;
        if let Some(comment_pos) = s.find("//") {
            s1 = s.split_at(comment_pos).0.trim();
        } else {
            s1 = s.trim();
        }
        if s1.is_empty() {
            return MapInfoLine::Empty;
        }
        let parse_r = mapinfo_line(s1);
        match parse_r {
            Ok(ml) => ml.1,
            Err(_) => {
                // println!("Warning: can't parse mapinfo line: `{}'", s);
                MapInfoLine::Empty
            }
        }
    }
}

fn title(i: &str) -> IResult<&str, MapInfoLine> {
    let (input, _) = tag("title ")(i)?;
    let (input, title) = not_line_ending(input)?;
    Ok((input, MapInfoLine::Title(title)))
}

fn author(i: &str) -> IResult<&str, MapInfoLine> {
    let (input, _) = tag("author ")(i)?;
    let (input, title) = not_line_ending(input)?;
    Ok((input, MapInfoLine::Author(title)))
}

fn description(i: &str) -> IResult<&str, MapInfoLine> {
    let (input, _) = tag("description ")(i)?;
    let (input, title) = not_line_ending(input)?;
    Ok((input, MapInfoLine::Description(title)))
}

fn cdtrack(i: &str) -> IResult<&str, MapInfoLine> {
    let (input, _) = tag("cdtrack ")(i)?;
    let (input, title) = not_line_ending(input)?;
    Ok((input, MapInfoLine::Cdtrack(title)))
}

fn feature(i: &str) -> IResult<&str, MapInfoLine> {
    let (input, _) = tag("has ")(i)?;
    let (input, feature) = not_line_ending(input)?;
    Ok((input, MapInfoLine::Feature(feature)))
}

fn flag(i: &str) -> IResult<&str, MapInfoLine> {
    let (input, flag) = alt((
        tag("hidden"),
        tag("forbidden"),
        tag("frustrating"),
        tag("noautomaplist"),
    ))(i)?;
    Ok((input, MapInfoLine::Flag(flag)))
}

fn gametype(i: &str) -> IResult<&str, MapInfoLine> {
    let (input, _) = alt((tag("type"), tag("gametype")))(i)?;
    let (input, gt) = not_line_ending(input)?;
    Ok((input, MapInfoLine::GameType(gt)))
}

fn fog(i: &str) -> IResult<&str, MapInfoLine> {
    let (input, _) = tag("fog ")(i)?;
    let (input, title) = not_line_ending(input)?;
    Ok((input, MapInfoLine::Fog(title)))
}

fn size(i: &str) -> IResult<&str, MapInfoLine> {
    let (input, _) = tag("size ")(i)?;
    let (input, (mins_x, _, mins_y, _, mins_z, _, maxs_x, _, maxs_y, _, maxs_z)) = tuple((
        digit1, space1, digit1, space1, digit1, space1, digit1, space1, digit1, space1, digit1,
    ))(input)?;
    Ok((
        input,
        MapInfoLine::Size(
            mins_x.parse().unwrap(),
            mins_y.parse().unwrap(),
            mins_z.parse().unwrap(),
            maxs_x.parse().unwrap(),
            maxs_y.parse().unwrap(),
            maxs_z.parse().unwrap(),
        ),
    ))
}

fn quoted_or_maybe_not(i: &str) -> IResult<&str, &str> {
    alt((
        delimited(tag("\""), take_till(|x| x == '"'), tag("\"")),
        preceded(tag("\""), take_till(|x: char| x.is_ascii_whitespace())),
        take_till(|x: char| x.is_ascii_whitespace()),
    ))(i)
}

fn settemp_cvar(i: &str) -> IResult<&str, (&str, &str)> {
    let (input, (cvar, _, value)) = tuple((quoted_or_maybe_not, space1, quoted_or_maybe_not))(i)?;
    Ok((input, (cvar, value)))
}

// fn settemp_include(i: &str) -> IResult<&str, &str> {
//     preceded(
//         alt((tag("\"#include\" "), tag("\"include"), tag("#include"))),
//         quoted_or_maybe_not,
//     )(i)
// }

fn settemp(i: &str) -> IResult<&str, MapInfoLine> {
    let (input, (_, _, t, _, (var, val))) = tuple((
        tag("settemp_for_type"),
        space1,
        take_till(|x: char| x.is_ascii_whitespace()),
        space1,
        settemp_cvar,
    ))(i)?;
    Ok((input, MapInfoLine::SetTempForType(t, var, val)))
}

fn client_settemp(i: &str) -> IResult<&str, MapInfoLine> {
    let (input, (_, _, t, _, (var, val))) = tuple((
        tag("clientsettemp_for_type"),
        space1,
        take_till(|x: char| x.is_ascii_whitespace()),
        space1,
        settemp_cvar,
    ))(i)?;
    Ok((input, MapInfoLine::ClientSetTempForType(t, var, val)))
}

fn mapinfo_line(i: &str) -> IResult<&str, MapInfoLine> {
    alt((
        title,
        description,
        author,
        cdtrack,
        feature,
        flag,
        gametype,
        size,
        fog,
        settemp,
        client_settemp,
    ))(i)
}
